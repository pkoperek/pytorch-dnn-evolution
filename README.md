[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/pkoperek/pytorch-dnn-evolution)

[![pipeline status](https://gitlab.com/pkoperek/pytorch-dnn-evolution/badges/master/pipeline.svg)](https://gitlab.com/pkoperek/pytorch-dnn-evolution/commits/master)

## torch-dnnevo 

This module provides a framework for experimentation with DNN evolution. It is 
based on great foundation built by projects like `pytorch`, `DEAP` and 
`celery`. Evaluation of individual DNNs can scheduled on a cluster of machines.
Kubernetes deployment descriptors are provided in the `deploy` directory.

A simple web UI is provided to control the progress of evolution. All
individuals are stored in sqlite for further examination.

## Using gitpod.io

Since the development environment is cleanly replicated as a container, this is a recommended approach
to development. To open an instance of gitpod IDE, please click this link: 
[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/pkoperek/pytorch-dnn-evolution)
and simply follow the steps to create an account. Since the project is open-source you don't have 
to worry about costs :)

### Using docker in gitpod.io

* Start the workspace.
* In a terminal tab run `$ sudo docker-up` and wait until it initializes (if necessary 
  create a new terminal with "Terminal -> New Terminal" command in the menu).
* Open a new terminal (e.g. use the "Terminal -> New Terminal" command in the menu). 
  In this new terminal all docker commands (`docker`, `docker-compose` etc) should work

Note: in most recent version of `.gitpod.yml` file the above should be automated, however if 
that does not work, you can also prep up the workspace manually.

## Offline development setup

* Install `docker` and `docker-compose`
* To build docker image from local files run: <br/>
  `docker build . -t pkoperek/pytorch-dnnevo:latest`
* You can launch all of the containers with `docker-compose up`
* Changes to driver code trigger automatic reload of the driver webapp - makes
  it easier to develop UI.

Project is automatically setup if you are using `direnv`.

To be able to run unit tests install deps with: `pip3 install -r requirements.txt`

## MiniKube setup

* Install minikube
* Start minikube cluster: `$ minikube start`
* Deploy app to minikube: `$ minikube deploy -f tools/kubernetes/dnnevo.yml`

## Experiment configuration parameters
### Parameters in JSON config
- batch_size: number of images to be used at once during training and evaluation;
- convolution_size_multiplier: factor adjusting output channels of convolutional layers in neural networks (main population individuals);
- dataset: name of the dataset (which defines the problem to be solved);
- dense_size_multiplier: factor adjusting output features of dense layers in neural networks (main population individuals);
- learning_rate: learning rate in gradient descent used in neural network training;
- momentum: an argument of gradient descent optimizer;
- optimizer: optimizer for neural network training;
- rng_seed: seed used in methods using randomization;
- train_iterations: number of iterations of neural network training using gradient descent;
- fp_crossover_probability: probability of replacing two fitness predictors with their offspring;
- fp_individual_size: number of genes (images from the dataset) representing a fitness predictor;
- fp_individual_mutation_probability: probability of letting an individual from fitness predictors' population mutate its genotype;
- fp_attribute_mutation_probability: probability of changing a single value (one image) in a fitness predictor's genotype during its mutation;
- fp_population_size: number of fitness predictors in their population;
- main_crossover_probability: probability of replacing two neural networks with their offspring;
- main_individual_size: number of genes representing an individual from main population (a neural network). Every gene is represented by 5 floating point values;
- main_individual_mutation_probability: probability of letting an individual from main population mutate its genotype;
- main_attribute_mutation_probability: probability of changing a single value in main population's individual's genotype during its mutation;
- main_population_size: number of neural networks inside the main population;
- stale_iterations_cnt: whenever the experiment has yielded the same maximum accuracy for number of iterations equal to stale_iterations_cnt, a new fitness predictors' trainer is being selected;
- novelty: starting weight of novelty in evaluation. Ranges from 0 to 1. Weight of accuracy is equal to `1 - novelty`;
- novelty_genotype: boolean value deciding whether novelty should be calculated on the genotype. If false, novelty will be calculated on the individual's accuracy per class instead;
- novelty_archive_type: defines the algorithm used for novelty calculation;
- novelty_compare_to: defines comparison method inside an archive. Possible values are 'prev gens', 'all' and 'all besides self', which mean respectively: compare to individuals from previous generations only, compare to all individuals from both previous and current generations (including self), compare to all individuals that ever existed excluding self;
- experiment-name: name for an experiment (given to distinguish between experiments);

### Parameters outside of JSON config
These parameters should not be included inside the JSON Config text field. Instead, insert them into their separate field boxes.
- max-iterations: number of iterations of an experiment (epochs of evolution);
- is-local: defines whether the experiment should be conducted locally or remotely. Local experiments don't use database, web app, etc.;

## Sample experiment

### JSON Config
```json
{
  "fp_crossover_probability": 0.75,
  "fp_individual_size": 1000,
  "fp_individual_mutation_probability": 0.02,
  "fp_attribute_mutation_probability": 0.02,
  "fp_population_size": 32,
  "main_crossover_probability": 0.75,
  "main_individual_size": 16,
  "main_individual_mutation_probability": 0.02,
  "main_attribute_mutation_probability": 0.02,
  "main_population_size": 64,
  "stale_iterations_cnt": 2,

  "novelty": 50,
  "novelty_genotype": true,
  "novelty_fading_factor": 0,
  "novelty_archive_type": "knn",
  "novelty_compare_to": "all besides self",

  "main_train_config": {
    "batch_size": 128,
    "convolution_size_multiplier": 8,
    "dataset": "MNIST",
    "dense_size_multiplier": 4,
    "learning_rate": 0.1,
    "momentum": 0.9,
    "optimizer": "adam",
    "rng_seed": 1,
    "train_iterations": 10
  },
  "fp_train_config": {
    "batch_size": 128,
    "convolution_size_multiplier": 8,
    "dataset": "MNIST",
    "dense_size_multiplier": 4,
    "learning_rate": 0.1,
    "momentum": 0.9,
    "optimizer": "adam",
    "rng_seed": 0,
    "train_iterations": 10
  }
}
```
### Separate parameters
Experiment Name: experiment1

Max iterations: 5

## Architecture

The system consists of following parts:

* The library responsible for performing the evolution (the `lib/` directory).
* The driver application (the `ui/` directory).
* The workers cluster (jobs are executed through Celery; as per
  https://github.com/docker-library/celery/issues/1#issuecomment-287655769 the
  official way to create a Celery-enabled worker Docker containers is to just
  add Celery dep to app container and run the container with a different
  command). Link to official, deprecated celery image:
  `https://hub.docker.com/_/celery/`. The worker is also a part of the `ui/` component.