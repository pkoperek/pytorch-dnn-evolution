FROM python:3.10 AS base
MAINTAINER pkoperek@gmail.com

COPY requirements.txt /tmp/requirements.txt

# Install dependencies
RUN apt-get update && apt-get install --no-install-recommends -y\
    graphviz\
    libev-dev\
    && rm -rf /var/lib/apt/lists/* \
    && pip3 install --no-cache-dir -r /tmp/requirements.txt

RUN mkdir -p \
    /datasets/mnist \
    /datasets/cifar10 \
    /srv/frontend \
    /tmp/lib \
    /tmp/ui

# Prepare datasets
ENV PDE_DATASETS_PATH "/datasets"
COPY tools/prepare_datasets.py /tmp
RUN cd /tmp \
    && python3 prepare_datasets.py \
    && rm /datasets/cifar10/cifar-10-python.tar.gz

# Build UI
FROM node:13 AS ui_build

RUN mkdir /tmp/frontend
COPY ui/frontend/package*.json /tmp/frontend/
RUN cd /tmp/frontend && npm install
COPY ui/frontend/ /tmp/frontend-src
RUN mv /tmp/frontend-src/* /tmp/frontend \
    && rm -rf /tmp/frontend-src \
    && cd /tmp/frontend \
    && npm run-script build

# Prepare UI
FROM base AS release

ARG version="default"
WORKDIR /srv
COPY --from=ui_build /tmp/frontend/build /srv/frontend

COPY lib/ /tmp/lib
RUN cd /tmp/lib \
    && python3 setup.py install \
    && rm -rf /tmp/lib

COPY ui/ /tmp/ui
RUN cd /tmp/ui \
    && python3 setup.py install \
    && mv /tmp/ui/start_worker.sh /srv \
    && rm -rf /tmp/ui

ENV VERSION=$version

CMD ["evomgr"]
