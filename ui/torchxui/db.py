import io
import os
from typing import Optional

from sqlalchemy import (
    Column,
    Boolean,
    ForeignKey,
    Integer,
    Numeric,
    String,
    JSON,
    ARRAY,
    create_engine,
)
from sqlalchemy.engine import Connection
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.sql import func

Base = declarative_base()


class Job(Base):
    __tablename__ = 'jobs'

    id = Column(Integer, primary_key=True)
    submission_delay = Column(Integer)
    mi = Column(Numeric)
    number_of_cores = Column(Integer)
    cpu_time_spent_s = Column(Numeric)
    mips_per_core = Column(Numeric)
    wallclock_time_spent_s = Column(Numeric)
    job_started = Column(Numeric)
    job_ended = Column(Numeric)
    extra_info = Column(String)


class Experiment(Base):
    __tablename__ = 'experiments'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    date = Column(String)
    max_iterations = Column(Integer)
    iteration_no = Column(Integer)
    configuration_json = Column(JSON)
    deleted = Column(Boolean, default=False)
    populations = relationship('Population', backref='experiment')
    archive_entries = relationship('ArchiveEntry', backref='experiment')


class Population(Base):
    __tablename__ = 'populations'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    iterations = relationship('Iteration', backref='population')
    experiment_id = Column(Integer, ForeignKey('experiments.id'))


class Iteration(Base):
    __tablename__ = 'iterations'

    id = Column(Integer, primary_key=True)
    current_target = Column(Integer)
    iteration_no = Column(Integer)
    population_id = Column(Integer, ForeignKey('populations.id'))
    individuals = relationship('Individual', backref='iteration')


class Individual(Base):
    __tablename__ = 'individuals'

    id = Column(Integer, primary_key=True)
    iteration_id = Column(Integer, ForeignKey('iterations.id'))
    fitness = Column(Numeric)
    accuracy = Column(Numeric)
    novelty = Column(Numeric)
    genotype = Column(String)
    evaluation_time = Column(Numeric)
    parents = Column(String)
    genealogy_index = Column(Integer)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
    
class ArchiveEntry(Base):
    __tablename__ = 'archive_entries'

    id = Column(Integer, primary_key=True)
    experiment_id = Column(Integer, ForeignKey('experiments.id'))
    entry = Column(ARRAY(Numeric))


db_host = os.environ.get('DB_HOST', 'postgres')
db_port = os.environ.get('DB_PORT', '5432')
db_name = os.environ.get('DB_NAME', 'dnnevo')
db_user = os.environ.get('DB_USER', 'dnnevo')
db_pass = os.environ.get('DB_PASS', 'dnnevo')

engine: Optional[Connection] = None


def initialize_db_engine():
    global engine
    test_env = os.environ.get('TEST_ENV', '')
    if not test_env:
        engine = create_engine(
            f'postgresql://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}',
        )
    Base.metadata.create_all(engine)


def load_model_genotype(session, model_id):
    wrapped = session.query(Individual.genotype).filter_by(id=model_id).first()
    return wrapped[0]


def load_database(db_dump):
    if engine is None:
        initialize_db_engine()
    result = engine.execute(db_dump)

    str_result = ""
    try:
        for line in result:
            str_result += str(line) + '\n'
    finally:
        result.close()

    return str_result


def dump_database():
    if engine is None:
        initialize_db_engine()
    conn = engine.raw_connection()
    dump = io.BytesIO()
    for line in conn.iterdump():
        line_with_newline = line + '\n'
        dump.write(line_with_newline.encode('utf-8'))
    dump.seek(0)
    return dump


# Refactor: wrap in a class
#
def get_db_session():
    if engine is None:
        initialize_db_engine()
    bound_sessionmaker = sessionmaker(bind=engine)
    session = bound_sessionmaker()
    return session


def get_experiments(session, offset: int, limit: int = 10):
    if offset < 0:
        raise RuntimeError('Offset has to be > 0!')
    return session.query(Experiment).where(Experiment.deleted == False).limit(limit).offset(offset).all()


def get_experiment(session, experiment_id):
    return session.query(Experiment).get(experiment_id)


def get_archive_entries(session, experiment_id):
    return session.query(ArchiveEntry.entry).where(Experiment.id == experiment_id).all()


def list_experiments(session):
    experiments = get_experiments(session, offset=0, limit=20)
    experiments_list = [{
        'id': experiment.id,
        'name': experiment.name,
        'max_iter': experiment.max_iterations,
        'iter_no': experiment.iteration_no
    } for experiment in experiments]
    return experiments_list


def experiment_to_dict(session, experiment: Experiment):
    best_individual = session.query(Individual).join(Iteration).join(Population)\
                            .where(Population.name == 'main', Population.experiment_id == experiment.id)\
                            .order_by(Individual.accuracy.desc()).all()[0]
    
    def get_metric_statistics(metric_name: str, population_name: str):
        metric = getattr(Individual, metric_name)

        metric_data = (
            session.query(
                Iteration.iteration_no,
                func.max(metric),
                func.min(metric),
                func.avg(metric),
                func.coalesce(func.stddev(metric), 0),
            )
            .join(Iteration)
            .join(Population)
            .where(Population.name == population_name, Population.experiment_id == experiment.id)
            .group_by(Iteration.iteration_no)
            .order_by(Iteration.iteration_no)
            .all()
        )

        return {
            "labels": ["gen", "max", "min", "avg", "std"],
            "values": list(map(list, zip(*metric_data))),
        }

    return {
        'id': experiment.id,
        'name': experiment.name,
        'highestAccuracy': best_individual.accuracy,
        'bestIndividual': str(best_individual.genotype),
        'date': experiment.date,
        'results': {
            'main': {
                'accuracy': get_metric_statistics('accuracy', 'main'),
                'fitness': get_metric_statistics('fitness', 'main'),
                'novelty': get_metric_statistics('novelty', 'main'),
            },
            'fp': {
                'fitness': get_metric_statistics('fitness', 'fp'),
            }
        }
    }


def filter_individuals(session, params: dict):
    individuals = session.query(Individual).join(Iteration).join(Population)\
        .join(Experiment).where(Experiment.deleted == False)\
        .where(Population.name == 'main')

    if 'experiment_id' in params.keys() and params['experiment_id'] is not None:
        individuals = individuals.where(Population.experiment_id == params['experiment_id'])
    if 'iteration_id' in params.keys() and params['iteration_id'] is not None:
        individuals = individuals.where(Iteration.id == params['iteration_id'])
    if 'population_id' in params.keys() and params['population_id'] is not None:
        individuals = individuals.where(Population.id == params['population_id'])
    if 'min_accuracy' in params.keys() and params['min_accuracy'] is not None:
        individuals = individuals.where(Individual.fitness >= params['min_accuracy'])

    return individuals


def get_individuals(session, offset, params: dict, limit=10):
    if offset < 0:
        raise RuntimeError('Offset has to be > 0!')

    return filter_individuals(session, params).limit(limit).offset(offset).all()


def get_individuals_count(session, params: dict):
    return filter_individuals(session, params).count()


def get_individual_with_config(session, individual_id):
    return session.query(Individual, Experiment.configuration_json)\
        .filter(Individual.id == individual_id)\
        .join(Iteration)\
        .join(Population)\
        .join(Experiment).all()[0]


def get_population_name_for_individual_id(session, individual_id):
    return session.query(Population.name).select_from(Individual).join(Iteration).join(
        Population).filter(Individual.id == individual_id).first()


def remove_experiment(experiment_id):
    # open a separate session, because there is high probability of concurrent query -> causing errors
    update_session = get_db_session()
    with update_session.begin():
        update_session.query(Experiment).filter(Experiment.id == experiment_id).update({'deleted': True})


def engine_dispose():
    if engine is not None:
        engine.dispose()
