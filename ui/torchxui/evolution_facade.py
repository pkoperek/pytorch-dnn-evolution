import datetime as dt
import logging
import os
from threading import Thread

from torchxui.coevo_state import Status, CoEvolutionState

VERSION = os.environ.get('VERSION', 'default')

logger = logging.getLogger(__name__)
logger.info(f'Running version: {VERSION}')


class EvolutionFacade(object):

    def __init__(self):
        self.evolution_state = None
        self.evolution_thread = None
        self._progress_listeners = []
        self._fitness_listeners = []

    def register_progress_listener(self, listener):
        self._progress_listeners.append(listener)

    def register_fitness_listener(self, listener):
        self._fitness_listeners.append(listener)

    def status_str(self):
        if self._is_initialized():
            status = self.evolution_state.status
        else:
            status = Status.NOT_STARTED

        status_str = str(status).replace("Status.", "")
        return status_str

    def iteration_progress_str(self):
        if self._is_initialized():
            return str(self.evolution_state.iteration_progress)

        return "0"

    def experiment_name_str(self):
        if self._is_initialized():
            return str(self.evolution_state.name)

        return ""

    def iteration_no(self):
        if self.evolution_state is not None:
            return self.evolution_state.iteration_no

        return -1

    def state(self):
        update_timestamp_str = dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        return {
            'update_timestamp': update_timestamp_str,
            'experiment_name': self.experiment_name_str(),
            'status': self.status_str(),
            'code_version': VERSION,
        }

    def statistics_history(self):
        if self.evolution_state.status == Status.FINISHED or self._is_initialized():
            return self.evolution_state.statistics_history()

        return {
            "main": {
                "values": {
                    "fitness": [[], [], [], [], []],
                    "accuracy": [[], [], [], [], []],
                    "novelty": [[], [], [], [], []]
                },
                "labels": ["gen", "max", "min", "avg", "std"]
            },
            "fp": {
                "values": {
                    "fitness": [[], [], [], [], []]
                },
                "labels": ["gen", "max", "min", "avg", "std"]
            }
        }

    def _is_initialized(self):
        return self.evolution_state is not None and self.evolution_state.status != Status.FINISHED

    def start_evolution(self, config_dict, db_session, zk_client):
        if not self._is_initialized():
            self.evolution_state = CoEvolutionState(config_dict, db_session, zk_client)
            self.evolution_state.register_progress_listener(self)
            self.evolution_state.register_fitness_listener(self)
            self.evolution_thread = Thread(target=self.evolution_state)
            self.evolution_thread.start()
            return True

        return False

    def stop_evolution(self):
        if self._is_initialized():
            self.evolution_state.interrupt()
            self.evolution_state = None

            self.evolution_thread.join(5.0)
            self.evolution_thread = None
            return True

        return False

    def progress(self):
        if self._is_initialized():
            return self.evolution_state.progress()

        return {
            'evolution_progress': 0,
            'iteration_progress': 0,
            'iteration_name': 'None'
        }

    def handle_progress_update(self, progress):
        for progress_listener in self._progress_listeners:
            progress_listener.handle_update(progress)

    def handle_fitness_update(self, fitness_update):
        for fitness_listener in self._fitness_listeners:
            fitness_listener.handle_update(fitness_update)
