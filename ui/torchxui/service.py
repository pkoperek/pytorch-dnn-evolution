import json
import logging
import os
from threading import Timer
from typing import Callable

import eventlet
from flask import Flask, send_from_directory
from flask_socketio import SocketIO
from sqlalchemy.orm import Session

from torchx.evo.genotype import Genotype
from torchxui.db import list_experiments, get_individuals, get_individuals_count, get_individual_with_config,\
    get_population_name_for_individual_id, get_experiments, experiment_to_dict, remove_experiment
from torchxui.evolution_facade import EvolutionFacade
from torchxui.json import setup_json_serialization
from torchxui.zookeeper import DNNEvoZookeeperClient
from torchx.evo.intermediate import genotype_to_layers
from torchxui.layers_visualization import visualise_graph

logger = logging.getLogger(__name__)

eventlet.monkey_patch()
setup_json_serialization()


def genotype_from_json(json_str: str) -> Genotype:
    code = json.loads(json_str)
    return Genotype(code)


def initialize_flask() -> Flask:
    frontend_folder = '/srv/frontend'
    app = Flask(__name__, static_folder=frontend_folder)

    # todo: introduce external configuration
    app.config['SECRET_KEY'] = 's3cr3tk3y'

    @app.route('/')
    def root():
        return app.send_static_file('index.html')

    @app.route('/<path:path>')
    def send_static(path: str):
        logger.error(f'Requested path: {path}')
        return send_from_directory(frontend_folder, path)

    return app


def schedule(time_at: float, fn: Callable):
    Timer(time_at, fn).start()


def create_evolution_facade(socketio: SocketIO) -> EvolutionFacade:
    evolution_facade = EvolutionFacade()
    evolution_facade.register_progress_listener(
        SocketEmitListener(socketio, 'exp_progress_update')
    )
    evolution_facade.register_fitness_listener(
        SocketEmitListener(socketio, 'exp_statistics_update')
    )
    return evolution_facade


def initialize_socketio(app: Flask, db_session: Session, zk_client: DNNEvoZookeeperClient) -> SocketIO:
    cors_allowed_origins = os.environ.get('CORS_ALLOWED_ORIGIN')
    socketio = SocketIO(app, cors_allowed_origins=cors_allowed_origins)
    logger.info(f'Allowing the following origins for CORS: {cors_allowed_origins}')
    app.evolution = create_evolution_facade(socketio)

    def broadcast_status_update():
        socketio.emit('exp_status', app.evolution.state())
        schedule(5.0, broadcast_status_update)

    logger.info('Scheduling broadcasting experiment status')
    schedule(5.0, broadcast_status_update)

    @socketio.on_error()
    def error_handler(e):
        logger.error(f'Error caught {e}')
        logger.error(e)

    @socketio.on('connect')
    def handle_new_connection():
        logger.debug('new connection established')

    @socketio.on('exp_start')
    def handle_experiment_start(configuration_dict):
        logger.info(f'Starting evolution: configuration: {configuration_dict}')

        ret_val = app.evolution.start_evolution(
            config_dict=configuration_dict,
            db_session=db_session,
            zk_client=zk_client,
        )

        experiment_name = configuration_dict['experiment-name']
        if ret_val:
            logger.info(f'Experiment {experiment_name} started')
        else:
            logger.warning(
                f'Experiment {experiment_name} not started - another experiment is already running!')

    @socketio.on('exp_stop')
    def handle_experiment_stop():
        ret_val = app.evolution.stop_evolution()
        if ret_val:
            logger.info(f'Experiment {app.evolution.experiment_name_str()} stopped')
        else:
            logger.warning('No experiment stopped - nothing currently running')

    @socketio.on('exp_resume')
    def handle_experiment_resume(experiment_id):
        config_dict = {'experiment-id': experiment_id}
        ret_val = app.evolution.start_evolution(
            config_dict=config_dict,
            db_session=db_session,
            zk_client=zk_client,
        )
        if ret_val:
            logger.info(f'Experiment {app.evolution.experiment_name_str()} resumed')
        else:
            logger.warning('No experiment resumed - nothing currently running')

    @socketio.on('exp_delete')
    def handle_experiment_delete(experiment_id):
        remove_experiment(experiment_id["id"])
        logger.debug(f'Removed experiment: {experiment_id}')

        # send updates list of experiments
        experiments_list = list_experiments(db_session)
        socketio.emit('exp_list', experiments_list)

    @socketio.on('exp_list_request')
    def handle_list_experiments():
        experiments_list = list_experiments(db_session)
        socketio.emit('exp_list', experiments_list)
        logger.debug(f'Experiments list: {experiments_list}')

    @socketio.on('exp_statistics_init_request')
    def handle_history():
        history = app.evolution.statistics_history()
        socketio.emit('exp_statistics_init', history)

    @socketio.on('exp_progress_init_request')
    def handle_progress_init_request():
        progress = app.evolution.progress()
        socketio.emit('exp_progress_init', progress)

    saved_experiment_id = zk_client.get_experiment_id()
    logger.info(f'Saved experiment id: {saved_experiment_id}')

    # @socketio.on("fitness_socket_request")
    # def handle_fitness():
    #     if app.evolution.is_initialized():
    #         fitness = app.evolution.fitness()
    #     else:
    #         fitness = {
    #             'values': [],
    #             'labels': [],
    #         }
    #     response = {
    #         'fitness': fitness,
    #         'status': app.evolution.status()
    #     }
    #     socketio.emit("fitness_socket_response", response)

    # @socketio.on("fp_history_request")
    # def handle_fp_history():
    #     history = app.evolution_state.history
    #     if history is not None:
    #         fp_hist = history["fp"]
    #         graph = networkx.DiGraph(fp_hist.genealogy_tree)
    #         graph = graph.reverse()
    #         d = json_graph.node_link_data(graph)
    #         socketio.emit("fp_history_response", json.dumps(d))

    # @socketio.on("main_history_request")
    # def handle_main_history():
    #     history = app.evolution_state.history
    #     if history is not None:
    #         main_hist = history["main"]
    #         graph = networkx.DiGraph(main_hist.genealogy_tree)
    #         graph = graph.reverse()
    #         d = json_graph.node_link_data(graph)
    #         socketio.emit("main_history_response", json.dumps(d))

    # @socketio.on("dbimport")
    # def handle_dbimport(message):
    #     db_dump = message['dump']
    #     logger.info('Loading the data from provided JSON to the database')
    #     try:
    #         ret_val = load_database(db_dump)
    #     except Exception as e:
    #         ret_val = "Exception caught: " + str(e)
    #         logger.error('Exception caught when loading the database from '
    #                      'provided JSON!')
    #         logger.error(e)
    #     logger.info(ret_val)
    #     socketio.emit(
    #         "dbimport_response",
    #         json.dumps({'return_value': ret_val}),
    #     )

    @socketio.on("individuals_request")
    def handle_individuals(message):
        page = message['page'] if 'page' in message else 0
        individuals = get_individuals(db_session, page * 10, message)
        count = get_individuals_count(db_session, message)
        socketio.emit("individuals_response",
                      json.dumps(
                          {
                              'individuals': list(map(lambda x: x.as_dict(), individuals)),
                              'count': count
                          }
                      ))

    @socketio.on("individual_request")
    def handle_individual(message):
        individual_id = str(message['id']) if 'id' in message else 1
        individual, configuration = get_individual_with_config(db_session, individual_id)
        individual_json = individual.as_dict()
        configuration_json = json.loads(configuration)

        genotype_code = json.loads(individual.genotype)
        genotype = Genotype(genotype_code)
        layers = genotype_to_layers(genotype)

        individual_json['layers'] = layers
        type_pop = get_population_name_for_individual_id(db_session, individual_id)
        individual_json['type'] = tuple(type_pop)
        individual_json['genotype_formatted'] = genotype
        individual_json['layers_visualization'] = visualise_graph(genotype_code, configuration_json['main_train_config'])
        socketio.emit("individual_response",
                      json.dumps(individual_json))

    @socketio.on("experiments_request")
    def handle_experiments(message):
        page = message['page'] if 'page' in message else 0
        experiments = get_experiments(session=db_session, offset=page * 10)
        res_json = {'experiments': [experiment_to_dict(db_session, experiment) for experiment in experiments]}

        socketio.emit("experiments_response", json.dumps(res_json))

    return socketio


class SocketEmitListener(object):

    def __init__(self, socketio, event_name):
        self._socketio = socketio
        self._event_name = event_name

    def handle_update(self, progress):
        self._socketio.emit(self._event_name, progress)
