import networkx as nx
import matplotlib.pyplot as plt
import base64
from typing import Dict, Tuple, Any, List
from torchx.evo.intermediate import Layer
from torchx.evo.nn import translate_to_model


def get_edges_and_vertices(model, edges=None, vertices=None):
    if edges is None:
        edges: List[Tuple[str, str, Dict[str, Any]]] = []
    if vertices is None:
        vertices: Dict[int, Tuple[str, Dict[str, Any]]] = {}

    if model.get_node_id() not in vertices:
        vertices[model.get_node_id()] = (str(len(vertices)), {"graph_node": model})
    else:
        return edges, vertices.values()

    if len(model.dependencies) > 0:
        for dep in model.dependencies:
            get_edges_and_vertices(dep, edges, vertices)
            edges.append((vertices[dep.get_node_id()][0], vertices[model.get_node_id()][0], {"output_shape": dep.output_shape}))
    else:
        vertices[model.get_node_id()] = ('-1', vertices[model.get_node_id()][1])

    return edges, vertices.values()


def get_color_map():
    return {
        Layer.DENSE: '#4053d3',
        Layer.CONVOLUTION: '#00b25d'
    }


def visualise_graph(genotype, parameters):
    INPUT_DATA_COLOR = '#ddb310'
    OUTPUT_DATA_COLOR = '#fb49b0'

    nn_model = translate_to_model(genotype, parameters)

    edges, vertices = get_edges_and_vertices(nn_model.exit_node)

    graph = nx.DiGraph()
    graph.add_nodes_from(vertices)
    graph.add_node('IN')
    graph.add_node('OUT')
    graph.add_edges_from(edges)
    graph.add_edge('IN', '-1', output_shape=nn_model.exit_node.network_input_shape)
    graph.add_edge('0', 'OUT', output_shape=(nn_model.exit_node.network_output_size,))

    color_map = get_color_map()
    node_colors = [color_map[vertex[1]["graph_node"].layer_type] for vertex in vertices]
    node_colors.append(INPUT_DATA_COLOR)
    node_colors.append(OUTPUT_DATA_COLOR)

    pos = nx.spring_layout(graph)

    node_labels = {'IN': 'input', 'OUT': 'output'}
    nx.draw_networkx_labels(graph, pos=pos, labels=node_labels)
    nx.draw(graph, pos, node_color=node_colors, node_size=1500, edge_color='gray', edge_cmap=plt.cm.Reds)
    edge_labels = nx.get_edge_attributes(graph, "output_shape")
    nx.draw_networkx_edge_labels(graph, pos, edge_labels)

    image_name = "grap-temp.png"
    plt.savefig(image_name)
    with open(image_name, 'rb') as file:
        img = file.read()
        return base64.encodebytes(img).decode('utf-8')
