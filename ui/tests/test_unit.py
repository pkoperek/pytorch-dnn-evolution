import os
import time
import unittest
from typing import Callable, List

from alchemy_mock.mocking import UnifiedAlchemyMagicMock
from sqlalchemy.orm import Session
from torchxui.db import Experiment, Population
from torchxui.service import initialize_flask, initialize_socketio

os.environ['TEST_ENV'] = 'true'
test_exp_config = {
    'evo-config': "{\"fp_crossover_probability\":\"0.1\",\"fp_individual_size\":\"8\",\"fp_mutation_probability\":\"0.5\",\"fp_population_size\":\"4\",\"main_crossover_probability\":\"0.1\",\"main_individual_size\":\"8\",\"main_mutation_probability\":\"0.1\",\"main_population_size\":\"4\",\"stale_iterations_cnt\":\"1\",\"fp_train_config\":{\"batch_size\":\"128\",\"convolution_size_multiplier\":\"2\",\"dataset\":\"MNIST\",\"dense_size_multiplier\":\"1\",\"learning_rate\":\"0.1\",\"momentum\":\"0.9\",\"optimizer\":\"adam\",\"rng_seed\":\"0\",\"train_iterations\":\"1\"},\"main_train_config\":{\"batch_size\":\"128\",\"convolution_size_multiplier\":\"2\",\"dataset\":\"MNIST\",\"dense_size_multiplier\":\"1\",\"learning_rate\":\"0.1\",\"momentum\":\"0.9\",\"optimizer\":\"adam\",\"rng_seed\":\"1\",\"train_iterations\":\"1\"}}",
    'experiment-name': 'test',
    'max-iterations': '10'
}


class MockDBSession(Session):
    class ExperimentQuery:
        def __init__(self, data: List[Experiment]):
            self.data = data

        def all(self):
            return self.data

        def get(self, key: dict):
            results = list(filter(lambda x: x.id == key['id'], self.data))
            return results[0] if len(results) > 0 else None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.session = UnifiedAlchemyMagicMock()
        self.experiments = []

    def add_experiment(self):
        i = len(self.experiments) + 1
        self.session.add(Experiment(
            id=i,
            name=f"{test_exp_config['experiment-name']}{i}",
            max_iterations=test_exp_config['max-iterations'],
            configuration_json=test_exp_config['evo-config'],
            iteration_no=0,
            populations=[
                Population(name="main", iterations=[]),
                Population(name="fp", iterations=[])
            ]
        ))

    def query(self, obj):
        if obj == Experiment:
            return self.ExperimentQuery(self.session.query(obj).all())
        else:
            return self.session.query(obj)


class MockDNNEvoZookeeperClient(object):
    experiment_id = ''

    def elect_leader(self, instance_id: str, callback: Callable):
        pass

    def get_experiment_id(self) -> str:
        return self.experiment_id

    def set_experiment_id(self, experiment_id: int):
        self.experiment_id = str(experiment_id)


class TestDNNEvoService(unittest.TestCase):

    def setUp(self):
        self.zk_client = MockDNNEvoZookeeperClient()
        self.db_session = MockDBSession()
        self.app = initialize_flask()
        self.socketio = initialize_socketio(self.app, self.db_session, self.zk_client)

    def test_connect(self):
        client1 = self.socketio.test_client(self.app)
        client2 = self.socketio.test_client(self.app)
        self.assertTrue(client1.is_connected())
        self.assertTrue(client2.is_connected())
        self.assertNotEqual(client1.sid, client2.sid)
        client1.disconnect()
        self.assertFalse(client1.is_connected())
        self.assertTrue(client2.is_connected())
        client2.disconnect()
        self.assertFalse(client2.is_connected())

    def test_exp_status(self):
        # expected results
        expected_state = 'NOT_STARTED'
        # test
        client = self.socketio.test_client(self.app)
        client.get_received()
        time.sleep(5.0)
        received = client.get_received()
        received = received[0]['args'][0]['status']
        self.assertEqual(received, expected_state)

    def test_exp_start(self):
        # expected results
        expected_state = 'RUNNING'
        # test
        client = self.socketio.test_client(self.app)
        client.get_received()
        client.emit('exp_start', test_exp_config)
        time.sleep(5.0)
        received = client.get_received()
        received = received[0]['args'][0]['status']
        self.assertEqual(received, expected_state)

    def test_exp_stop(self):
        # expected results
        expected_state = 'NOT_STARTED'
        # test
        client = self.socketio.test_client(self.app)
        client.get_received()
        # start experiment
        client.emit('exp_start', test_exp_config)
        # and then stop it
        client.emit('exp_stop')
        time.sleep(5.0)
        received = client.get_received()
        received = received[0]['args'][0]['status']
        self.assertEqual(received, expected_state)

    def test_exp_resume(self):
        # create one experiment
        self.db_session.add_experiment()
        # expected results
        expected_state = 'RUNNING'
        # test
        client = self.socketio.test_client(self.app)
        client.get_received()
        client.emit('exp_resume', {'id': 1})
        time.sleep(5.0)
        received = client.get_received()
        received = received[0]['args'][0]['status']
        self.assertEqual(received, expected_state)

    def test_exp_resume_one(self):
        # create two experiments
        self.db_session.add_experiment()
        self.db_session.add_experiment()
        # expected results
        expected_state = 'RUNNING'
        expected_name = f"{test_exp_config['experiment-name']}{1}"

        client = self.socketio.test_client(self.app)
        client.get_received()
        # resume second experiment
        client.emit('exp_resume', {'id': 1})
        time.sleep(5.0)
        received = client.get_received()
        received = received[0]['args'][0]
        self.assertEqual(received['status'], expected_state)
        self.assertEqual(received['experiment_name'], expected_name)

    def test_exp_resume_fail(self):
        # no experiments saved
        # expected results
        expected_state = 'NOT_STARTED'
        # test
        client = self.socketio.test_client(self.app)
        client.get_received()
        client.emit('exp_resume', {'id': 1})
        time.sleep(5.0)
        received = client.get_received()
        received = received[0]['args'][0]['status']
        self.assertEqual(received, expected_state)

    def test_exp_list_request(self):
        self.db_session.add_experiment()
        # expected results
        expected = [{
            'id': 1,
            'name': f"{test_exp_config['experiment-name']}{1}",
            'max_iter': test_exp_config['max-iterations'],
            'iter_no': 0
        }]
        # test
        client = self.socketio.test_client(self.app)
        client.get_received()
        client.emit('exp_list_request')
        received = client.get_received()
        received = received[0]['args'][0]
        self.assertEqual(received, expected)

    def test_exp_list_request_empty(self):
        # no experiments saved
        # expected results
        expected = []
        # test
        client = self.socketio.test_client(self.app)
        client.get_received()
        client.emit('exp_list_request')
        received = client.get_received()
        received = received[0]['args'][0]
        self.assertEqual(received, expected)

    def test_exp_statistics_init_request(self):
        # expected results
        expected = {
            "main": {
                "values": [[], [], [], [], []],
                "labels": ["gen", "max", "min", "avg", "std"]
            },
            "fp": {
                "values": [[], [], [], [], []],
                "labels": ["gen", "max", "min", "avg", "std"]
            }
        }
        # test
        client = self.socketio.test_client(self.app)
        client.get_received()
        client.emit('exp_statistics_init_request')
        received = client.get_received()
        received = received[0]['args'][0]
        self.assertEqual(received, expected)

    def test_exp_progress_init_request(self):
        # expected results
        expected = {
            'evolution_progress': 0,
            'iteration_progress': 0,
        }
        # test
        client = self.socketio.test_client(self.app)
        client.get_received()
        client.emit('exp_progress_init_request')
        received = client.get_received()
        received = received[0]['args'][0]
        self.assertEqual(received, expected)


if __name__ == '__main__':
    import xmlrunner
    unittest.main(testRunner=xmlrunner.XMLTestRunner(outsuffix='unit'))
