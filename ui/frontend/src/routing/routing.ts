interface Paths {
    control: string,
    current: string,
    database: string,
    experiments: string,
    individuals: string,
    individual:string,
    root: string
}

export const paths: Paths = {
    control: '/control',
    current: '/current',
    database: '/database',
    experiments: '/experiments',
    individuals: '/individuals',
    individual: '/individuals/:id',
    root: "/",
};
