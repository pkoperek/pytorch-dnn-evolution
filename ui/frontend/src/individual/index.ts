export {default} from './IndividualViewContainer';

export interface Gene {
    id: string
    x: number
    y: number
    gene_type_value: string
    gene_type: string
    extra: string
}

export interface Layer {
    id: string
    inputs: string
    outputs: string
    extras: string
}

export interface Individual {
    id: string
    type: string
    fitness: number
    accuracy: number
    parents: string
    genealogy_index: string
    genotype: string
    iteration_id: number
    evaluation_time: string
    genotype_formatted: Gene[]
    layers: Layer[]
    layers_visualization: string
}
