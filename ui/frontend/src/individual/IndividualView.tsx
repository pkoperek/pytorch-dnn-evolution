import * as React from 'react';
import {Individual} from "./index";
import './Individual.css';

interface Props {
    individual: Individual
}

export const IndividualView: React.FC<Props> = (props) => {
    const legend: Array<{color: string, name: string}> =
        [{color: '#4053d3', name: 'Dense Layer'},
         {color: '#00b25d', name: 'Convolution Layer with MaxPooling'},
         {color: '#ddb310', name: 'Input data'},
         {color: '#fb49b0', name: 'Probabilities per class'}];

    return (
        <div>
            <h4>Individual properties</h4>
            <table className="table table-hover table-bordered searchable">
                <tbody>
                <tr>
                    <td><b>Global id</b></td>
                    <td>{props.individual.id}</td>
                </tr>
                <tr>
                    <td><b>Type</b></td>
                    <td>{props.individual.type}</td>
                </tr>
                <tr>
                    <td><b>Fitness</b></td>
                    <td>{props.individual.fitness}</td>
                </tr>
                <tr>
                    <td><b>Parents</b></td>
                    <td>{props.individual.parents}</td>
                </tr>
                <tr>
                    <td><b>Genealogy Index</b></td>
                    <td>{props.individual.genealogy_index}</td>
                </tr>
                <tr>
                    <td><b>Iteration.Id</b></td>
                    <td>{props.individual.iteration_id}</td>
                </tr>
                <tr>
                    <td><b>Evaluation Time</b></td>
                    <td>{props.individual.evaluation_time}</td>
                </tr>

                </tbody>
            </table>
            <h4>Genotype</h4>
            <table className="table table-hover table-bordered searchable">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Position</th>
                    <th>Type</th>
                    <th>Extra</th>
                </tr>
                </thead>
                <tbody>
                {props.individual.genotype_formatted.map(gene => {
                    return (<tr key={gene.id}>
                        <td><span className="gene-id">{gene.id}</span></td>
                        <td><span className="gene-position">({gene.x}, {gene.y})</span></td>
                        <td><span className="gene-{gene_type}">
                        {gene.gene_type_value} ({gene.gene_type})
                    </span></td>
                        <td><span className="gene-extra">{gene.extra}</span></td>
                    </tr>);
                })
                }
                </tbody>
            </table>
            <h4>Individual structure</h4>
            <table className="table table-hover table-bordered searchable">
                <thead>
                <tr>
                    <th>Layer Id</th>
                    <th>Inputs</th>
                    <th>Outputs</th>
                    <th>Extras</th>
                </tr>
                </thead>
                <tbody>
                {props.individual.layers
                    .map(layer => {
                        return (<tr key={Math.random()}>
                            <td>{layer.id}</td>
                            <td>{JSON.stringify(layer.inputs)}</td>
                            <td>{JSON.stringify(layer.outputs)}</td>
                            <td>{JSON.stringify(layer.extras)}</td>
                        </tr>);
                    })
                }
                </tbody>
            </table>
            <h4>Layers visualization</h4>
            <div style={{display: "flex"}}>
                <img src={`data:image/jpeg;base64,${props.individual.layers_visualization}`} alt='visualized layers'/>
                <div className={'legend'}>
                    <div>
                        <div className='legendTitle'>Legend</div>
                        {legend.map((el, index) => <div key={index} className='legendElement'>
                            <span style={{backgroundColor: el.color}} className='legendDot'/>
                            <div>{el.name}</div>
                        </div>)}
                    </div>
                    <div className='legendDescription'>Numbers on edges describe outputs shapes. In case of convolution the first number is number of channels.</div>
                </div>
            </div>
        </div>
    );
};
