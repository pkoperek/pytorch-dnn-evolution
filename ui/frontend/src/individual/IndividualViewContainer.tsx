import * as React from 'react';
import {IndividualView} from './IndividualView';
import {SocketContextProps, withSocketContext} from '../common';
import {RouteComponentProps} from "react-router";
import {Individual} from "./index";

interface State {
    individual: Individual
}

interface RouterParamProps {
    id: string;   // This one is coming from the router
}

interface Props extends SocketContextProps, RouteComponentProps<RouterParamProps> {
}

export class IndividualViewContainer extends React.Component<Props, State> {
    public constructor(props) {
        super(props);
        this.state = {individual: undefined};
        this.props.socket.emit("individual_request", {id: Number.parseInt(this.props.match.params.id, 10)});
        this.props.socket.on("individual_response", this.handleIndividual);
    }

    public componentWillUnmount() {
        this.props.socket.off("individual_response", this.handleIndividual);
    }

    public render() {
        return (<div>{!!this.state.individual && <IndividualView individual={this.state.individual}/>}</div>);
    }

    private handleIndividual = (message) => {
        const msg = JSON.parse(message);
        this.setState({individual: msg});
    }

}


export default withSocketContext(IndividualViewContainer);