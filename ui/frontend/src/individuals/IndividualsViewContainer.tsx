import * as React from "react";
import { IndividualsView } from "./IndividualsView";
import { SocketContextProps, withSocketContext } from "../common";
import PaginationContainer from "../pagination/PaginationContainer";
import {Individual} from "../individual";
import {ControlLabel, FormControl, FormGroup, Modal} from "react-bootstrap";

interface State {
  individuals: Individual[];
  count: number;
  currentPageNumber: number;
  open: boolean;
  experimentId: number;
  iterationId: number;
  populationId: number;
  minAccuracy: number;
}

export class IndividualsViewContainer extends React.Component<
  SocketContextProps,
  State
> {
  public constructor(props) {
    super(props);
    this.state = { individuals: [], count: 0, currentPageNumber: 0, open: false, experimentId: null,
        iterationId: null, populationId: null, minAccuracy: 0};
    this.getNextPage(this.state.currentPageNumber);
    this.props.socket.on("individuals_response", this.handleIndividual);
  }

  public componentWillUnmount() {
    this.props.socket.off("individuals_response", this.handleIndividual);
  }

  public render() {



    return (
        <div className='root'>
          <Modal show={this.state.open} onHide={() => this.setState({open: false})}>
            <Modal.Header>
              <Modal.Title className='modalTitle'>
                  Set filter parameters
                  <button onClick={() => this.setState({open: false})}>
                      Close
                  </button>
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <FormGroup className='formGroup'>
                    <ControlLabel>
                        Experiment id
                    </ControlLabel>
                    <FormControl
                        className='formInput'
                        value={this.state.experimentId}
                        // @ts-ignore
                        onChange={(event) => this.setState({experimentId: event.target.value})}
                    />
                </FormGroup>
                <FormGroup className='formGroup'>
                    <ControlLabel>
                        Iteration id
                    </ControlLabel>
                    <FormControl
                        className='formInput'
                        value={this.state.iterationId}
                        // @ts-ignore
                        onChange={(event) => this.setState({iterationId: event.target.value})}
                    />
                </FormGroup>
                <FormGroup className='formGroup'>
                    <ControlLabel>
                        Population id
                    </ControlLabel>
                    <FormControl
                        className='formInput'
                        value={this.state.populationId}
                        // @ts-ignore
                        onChange={(event) => this.setState({populationId: event.target.value})}
                    />
                </FormGroup>
                <FormGroup className='formGroup'>
                    <ControlLabel>
                        Minimum accuracy
                    </ControlLabel>
                    <div style={{display: 'flex', width: 'fit-content'}}>
                        <FormControl
                            type="range"
                            value={this.state.minAccuracy}
                            min={0}
                            max={100}
                            // @ts-ignore
                            onChange={(event) => this.setState({minAccuracy: event.target.value})}
                            style={{width: '350px'}}
                        />
                        <div className='rangeInput'>
                            <div style={{width: 'fit-content', height: 'fit-content'}}>
                                {this.state.minAccuracy}
                            </div>
                        </div>
                    </div>
                </FormGroup>
                <button className='button' onClick={() => this.filter()}>Filter</button>
            </Modal.Body>
        </Modal>
        <button className='button' onClick={() => this.setState({open: true})}>Filter</button>
        <IndividualsView individuals={this.state.individuals} />
        <PaginationContainer
          currentPageNumber={this.state.currentPageNumber}
          handler={this.getNextPage}
          pageCount={this.state.count / 10}
        />
      </div>
    );
  }

  public getNextPage = (page: number) => {
    this.setState({ currentPageNumber: page });
    this.props.socket.emit("individuals_request", { page, experiment_id: this.state.experimentId,
        iteration_id: this.state.iterationId, population_id: this.state.populationId, min_accuracy: this.state.minAccuracy });
  }

  private handleIndividual = (message: any) => {
    const msg = JSON.parse(message);
    this.setState({ individuals: msg.individuals, count: msg.count });
  }

  private filter = () => {
      this.setState({open: false});
      this.getNextPage(this.state.currentPageNumber);
      this.setState({ experimentId: null, iterationId: null, populationId: null, minAccuracy: 0 });
  }
}

export default withSocketContext(IndividualsViewContainer);
