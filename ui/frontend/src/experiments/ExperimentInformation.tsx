import * as React from 'react';
import {Experiment} from "./index";
import {Line} from 'react-chartjs-2';

function getOptions() {
    return {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            xAxes: [
                {
                    display: true,
                    labelString: "Iteration"
                }
            ],
            yAxes: [
                {
                    display: true,
                    ticks: {
                        max: 100,
                        min: 0
                    }
                }
            ]
        }
    };
}

function translateData(data: { labels: string[], values: number[][] }) {
    const iterationsIndex = data.labels.indexOf('gen');
    const colors: string[] = [
        "#ff6384", // red
        "#36a2eb", // blue
        "#cc65fe", // purple
        "#ffce56", // yellow
    ];

    const datasets = data.values.map((value, id) => {
        return {
            label: data.labels[id],
            data: value,
            borderColor: colors[id],
            borderWidth: 2,
            lineTension: 0.25,
            fill: false
        };
    });

    return {
        labels: data.values[iterationsIndex],
        datasets: datasets.filter((dataset) => dataset.label !== 'gen')
    };
}

export const ExperimentInformation: React.StatelessComponent<Experiment> = (props) => {
    // @ts-ignore
    const [open, setOpen] = React.useState(false);

    return (
        <div style={{width: "100%", boxSizing: "border-box"}}>
            <div className='experimentInformation'>
                {open ? <span className="material-symbols-outlined" onClick={() => setOpen(false)}
                              style={{textAlign: 'center', height: 'fit-content'}}>keyboard_arrow_up</span> : <
                    span className="material-symbols-outlined" onClick={() => setOpen(true)}
                         style={{textAlign: 'center', height: 'fit-content'}}>keyboard_arrow_down</span>}
                <div className="cell">{props.id}</div>
                <div className="cell">{props.name}</div>
                <div className="cell">{props.highestAccuracy}</div>
                <div className="cell">{props.bestIndividual}</div>
                <div className="cell">{props.date}</div>
            </div>
            {open && <div className='graphContainer'>
                <div className="chart-container" style={{width: "100%"}}>
                    <h1>Main population - fitness progress over time</h1>
                    <Line options={getOptions()} data={translateData(props.results.main.fitness)}/>
                </div>
                <div className="chart-container" style={{width: "100%"}}>
                    <h1>Main population - accuracy progress over time</h1>
                    <Line options={getOptions()} data={translateData(props.results.main.accuracy)}/>
                </div>
                <div className="chart-container" style={{width: "100%"}}>
                    <h1>Main population - novelty progress over time</h1>
                    <Line options={getOptions()} data={translateData(props.results.main.novelty)}/>
                </div>
                <div className="chart-container" style={{width: "100%"}}>
                    <h1>Fitness Predictors population - fitness progress over time</h1>
                    <Line options={getOptions()} data={translateData(props.results.fp.fitness)}/>
                </div>
            </div>}
        </div>
    );
};
