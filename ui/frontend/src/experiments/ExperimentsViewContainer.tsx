import * as React from 'react';
import {SocketContextProps, withSocketContext} from '../common';
import {ExperimentsView} from "./ExperimentsView";
import {Experiment} from "./index";

interface State {
  experiments: Experiment[];
}

class ExperimentsViewContainer extends React.Component<SocketContextProps, State> {
  public constructor(props) {
    super(props);
    this.state = { experiments: [] };
    this.props.socket.on("experiments_response", this.handleExperiments);
  }

  public componentWillUnmount() {
    this.props.socket.off("experiments_response", this.handleExperiments);
  }

  public componentDidMount() {
    this.props.socket.emit("experiments_request", {page: 0});
  }

    public render() {
    return <ExperimentsView experiments={this.state.experiments}/>;
  }

  private handleExperiments = (message: any) => {
    const msg = JSON.parse(message);
    this.setState({ experiments: msg.experiments });
  }
}

export default withSocketContext(ExperimentsViewContainer);
