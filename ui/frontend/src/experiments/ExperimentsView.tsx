import * as React from 'react';
import './Experiments.css';
import {ExperimentInformation} from "./ExperimentInformation";
import {Experiment} from "./index";

interface Props {
    experiments: Experiment[]
}

export const ExperimentsView: React.StatelessComponent<Props> = (props) => {
    return (
        <div style={{width: "100%", boxSizing: "border-box"}}>
            <div className="experimentsHeader">
                <div className={"cell"}>
                    id
                </div>
                <div className={"cell"}>
                    Experiment name
                </div>
                <div className={"cell"}>
                    Highest accuracy
                </div>
                <div className={"cell"}>
                    Best individual
                </div>
                <div className={"cell"}>
                    Date
                </div>
            </div>
            {props.experiments.map((experiment, id) =>
                <ExperimentInformation id={experiment.id}
                                       name={experiment.name}
                                       highestAccuracy={experiment.highestAccuracy}
                                       bestIndividual={experiment.bestIndividual}
                                       date={experiment.date} key={id} results={experiment.results}/>)}
        </div>
    );
};
