export { default } from './ExperimentsViewContainer';

export interface Experiment {
    id: number
    name: string
    highestAccuracy: number
    bestIndividual: string
    date: string
    results: {main: { accuracy: {labels: string[], values: number[][]},
                      novelty: {labels: string[], values: number[][]},
                      fitness: {labels: string[], values: number[][]} },
              fp: { fitness: {labels: string[], values: number[][]} }}
}
