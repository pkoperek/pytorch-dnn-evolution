import * as React from 'react';
import {NewExperiment} from './NewExperiment';
import {SocketContextProps, withSocketContext} from '../../common';

class NewExperimentContainer extends React.Component<SocketContextProps, any> {
    public render() {
        return (
            <NewExperiment createExperiment={this.createExperiment}/>
        );
    }

    private createExperiment = (config: any, name: string, maxIterations: number) => {
        this.props.socket.emit('exp_start', {
            'evo-config': config,
            'experiment-name': name,
            'max-iterations': maxIterations
        });
    }
}

export default withSocketContext(NewExperimentContainer);
