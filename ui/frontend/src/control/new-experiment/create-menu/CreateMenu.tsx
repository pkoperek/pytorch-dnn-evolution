import * as React from 'react';
import {Tab, Tabs} from 'react-bootstrap';
import {JsonExperimentCreator} from './JsonExperimentCreator';
import {FormExperimentCreator} from './FormExperimentCreator';

interface Props {
    createExperiment: (config: any, name: string, maxIterations: number) => void,
    onClose: () => void
}

export class CreateMenu extends React.Component<Props, any> {
    public render() {
        return (
            <React.Fragment>
                <Tabs defaultActiveKey={1}>
                    <Tab eventKey={1} title="Form">
                        <FormExperimentCreator onClose={this.props.onClose}
                                               createExperiment={this.props.createExperiment}/>
                    </Tab>
                    <Tab eventKey={2} title="JSON">
                        <JsonExperimentCreator onClose={this.props.onClose}
                                               createExperiment={this.props.createExperiment}/>
                    </Tab>
                </Tabs>
            </React.Fragment>
        );
    }
}
