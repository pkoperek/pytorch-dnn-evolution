import * as React from 'react';
import '../../Control.css';
import {Button, Col, ControlLabel, FormControl, FormGroup, Grid, Row} from 'react-bootstrap';
import {FormElement} from '../../../common/forms/FormElement';

const initialEvolutionProps = {
    evo_props: {
        fp_crossover_probability: 0.1,
        fp_individual_size: 8,
        fp_individual_mutation_probability: 0.02,
        fp_attribute_mutation_probability: 0.02,
        fp_population_size: 4,
        main_crossover_probability: 0.1,
        main_individual_size: 8,
        main_individual_mutation_probability: 0.02,
        main_attribute_mutation_probability: 0.02,
        main_population_size: 4,
        stale_iterations_cnt: 1,
    },
    main_population: {
        batch_size: 128,
        convolution_size_multiplier: 2,
        dataset: 'MNIST',
        dense_size_multiplier: 1,
        learning_rate: 0.1,
        momentum: 0.9,
        optimizer: 'adam',
        rng_seed: 1,
        train_iterations: 1,
    },
    fp_population: {
        batch_size: 128,
        convolution_size_multiplier: 2,
        dataset: 'MNIST',
        dense_size_multiplier: 1,
        learning_rate: 0.1,
        momentum: 0.9,
        optimizer: 'adam',
        rng_seed: 0,
        train_iterations: 1
    },
    novelty: {
        novelty: 0,
        novelty_genotype: false,
        novelty_fading_factor: 0,
        novelty_archive_type: 'knn',
        novelty_compare_to: 'all besides self'
    },
};

const archives = ['knn', 'abod', 'cblof', 'hbos', 'lof', 'loci', 'sos'];

export class FormExperimentCreator extends React.Component<any, any> {
    public constructor(props) {
        super(props);
        this.state = {
            ...initialEvolutionProps,
            experimentName: '',
            maxIterations: undefined,
        };
    }

    public render() {

        return (
            <Grid className="FormExperimentCreator-container">
                <Row style={{marginBottom: '15px'}}>
                    <Col md={2}>
                        <FormElement
                            controlId="name"
                            label="Experiment name"
                            value={this.state.experimentName}
                            onChange={this.onUpdateValue('experimentName')}
                        />
                    </Col>
                    <Col md={2}>
                        <FormElement
                            controlId="iter"
                            label="Max iterations"
                            value={this.state.maxIterations}
                            onChange={this.onUpdateValue('maxIterations')}
                        />
                    </Col>
                    <Col md={2}>
                        <FormGroup
                            controlId="novelty"
                        >
                            <ControlLabel>
                                Novelty
                            </ControlLabel>
                            <FormControl
                                type="range"
                                value={this.state[`novelty`][`novelty`]}
                                min={0}
                                max={100}
                                onChange={this.updateNoveltyProp('novelty', 'novelty')}
                            />
                        </FormGroup>
                    </Col>
                    <Col mdOffset={10} className="FormExperimentCreator-buttons-container">
                        <Button
                            bsSize="small"
                            bsStyle="primary"
                            className="JsonExperimentCreator-button"
                            onClick={this.createExperiment}
                        >
                            ACCEPT
                        </Button>
                        <Button
                            bsSize="small"
                            bsStyle="secondary"
                            className="JsonExperimentCreator-button"
                            onClick={this.props.onClose}
                        >
                            CANCEL
                        </Button>
                    </Col>
                </Row>
                {this.state.novelty.novelty > 0 &&
                <Row style={{marginBottom: '30px', paddingLeft: '15px', paddingRight: '15px'}}>
                    <Row style={{marginBottom: '15px'}}>
                        <Col md={3}>
                            Novelty configuration
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3}>
                            <FormGroup
                                controlId="novelty_genotype"
                            >
                                <ControlLabel>
                                    Calculate novelty using genotype
                                </ControlLabel>
                                <FormControl
                                    type="checkbox"
                                    checked={this.state[`novelty`][`novelty_genotype`]}
                                    onChange={this.updateNoveltyProp('novelty', 'novelty_genotype')}
                                    className='d-flex flex-column justify-content-center align-items-center'
                                />
                            </FormGroup>
                        </Col>
                        <Col md={2}>
                            <FormGroup
                                controlId="novelty_fading_factor"
                            >
                                <ControlLabel>
                                    Novelty fading factor
                                </ControlLabel>
                                <FormControl
                                    type="range"
                                    value={this.state[`novelty`][`novelty_fading_factor`]}
                                    min={0}
                                    max={100}
                                    onChange={this.updateProp('novelty', 'novelty_fading_factor')}
                                />
                            </FormGroup>
                        </Col>
                        <Col md={3}>
                            <label>Novelty comparison type</label>
                            <select onChange={this.updateProp('novelty', 'novelty_compare_to')}>
                                <option value="all besids self">All besides self</option>
                                <option value="all">All</option>
                                <option value="prev gens">Previous generations</option>
                            </select>
                        </Col>
                        <Col md={2}>
                            <label>Novelty archive type</label>
                            <select onChange={this.updateProp('novelty', 'novelty_archive_type')}>
                                {archives.map((archive: string, index: number) => 
                                    <option value={archive} key={index}>{archive.toUpperCase()}</option>)}
                            </select>
                        </Col>
                    </Row>
                </Row>}
                <Row>
                    <Col md={4} sm={12}>
                        <div className="FormExperimentCreator-section-title">
                            Evolution properties
                        </div>
                        {
                            Object.keys(initialEvolutionProps.evo_props).map((key) => (
                                this.renderFormElement('evo_props', key)
                            ))
                        }
                    </Col>
                    <Col md={4} sm={12}>
                        <div className="FormExperimentCreator-section-title">
                            Main population - DNN training properties
                        </div>
                        {
                            Object.keys(initialEvolutionProps.main_population).map((key) => (
                                this.renderFormElement('main_population', key)
                            ))
                        }
                    </Col>
                    <Col md={4} sm={12}>
                        <div className="FormExperimentCreator-section-title">
                            FP Population - DNN training properties
                        </div>
                        {
                            Object.keys(initialEvolutionProps.fp_population).map((key) => (
                                this.renderFormElement('fp_population', key)
                            ))
                        }
                    </Col>
                </Row>
            </Grid>
        );
    }

    private prepNoveltyProps() {
        const props = this.state.novelty;
        props.novelty_fading_factor = props.novelty_fading_factor / 100;
        return props;
    }

    private createExperiment = () => {
        this.props.createExperiment(
            JSON.stringify({
                ...this.state.evo_props,
                ...this.prepNoveltyProps(),
                fp_train_config: this.state.fp_population,
                main_train_config: this.state.main_population
            }),
            this.state.experimentName,
            this.state.maxIterations,
            this.state.novelty
        );
        this.props.onClose();
    }

    private onUpdateValue = (propName) => (event) => {
        event.persist();
        this.setState({
            [propName]: event.target.value
        });
    }

    private updateProp = (propGroup, propName) => (event) => {
        event.persist();
        this.setState((prevState) => ({
            [propGroup]: {
                ...prevState[propGroup],
                [propName]: event.target.value
            }
        }));
    }

    private updateNoveltyProp = (propGroup, propName) => (event) => {
        if (propName === 'novelty_genotype') {
            event.persist();
            this.setState((prevState) => ({
                [propGroup]: {
                    ...prevState[propGroup],
                    [propName]: !prevState[propGroup][propName]
                }
            }));
        }
        else {
            event.persist();
            this.setState((prevState) => ({
                [propGroup]: {
                    ...prevState[propGroup],
                    [propName]: event.target.value
                }
            }));
        }
    }

    private renderFormElement = (propGroup, propName) => {
        return (
            <FormElement
                controlId={`${propGroup}:${propName}`}
                label={propName}
                value={this.state[propGroup][propName]}
                onChange={this.updateProp(propGroup, propName)}
            />
        );
    }
}
