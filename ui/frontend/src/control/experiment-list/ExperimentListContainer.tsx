import * as React from 'react';
import {SocketContextProps, withSocketContext} from '../../common/index';
import {ExperimentListItem} from './ExperimentListItem';
import {Grid, Row} from 'react-bootstrap';

interface State {
    experiments: Array<{
        id: string,
        name: string,
        max_iter: string,
        iter_no: string,
    }>
}


class ExperimentListContainer extends React.Component<SocketContextProps, State> {

    public constructor(props: any) {
        super(props);
        this.state = {experiments: []};
        this.props.socket.on("exp_list", this.handleStatusUpdate);
    }

    public render() {
        return (
            <Grid>
                <Row><h3> Saved Experiments </h3></Row>
                <Row>
                    {this.state.experiments.map(item => (
                        <ExperimentListItem
                            id={item.id}
                            name={item.name}
                            max_iter={item.max_iter}
                            iter_no={item.iter_no}
                            resume={() => {
                                this.resume(item.id);
                            }}
                            delete={() => {
                                this.delete(item.id);
                            }}
                        />
                    ))}
                </Row>
                <hr className="ExperimentStatus-divider"/>
            </Grid>
        );
    }

    public componentWillUnmount() {
        this.props.socket.off("exp_list", this.handleStatusUpdate);
    }

    public componentWillMount() {
        this.props.socket.emit("exp_list_request");
    }

    private handleStatusUpdate = (message: any) => {
        this.setState({experiments: message});
        console.log(message);
    }

    private resume = (value: string) => {
        this.props.socket.emit("exp_resume", {id: value});
    }

    private delete = (value: string) => {
        this.props.socket.emit("exp_delete", {id: value});
    }
}

export default withSocketContext(ExperimentListContainer);
