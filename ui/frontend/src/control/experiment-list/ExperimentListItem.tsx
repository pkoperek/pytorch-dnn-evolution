import * as React from 'react';
import '../Control.css';
import {Button, Grid, Row} from 'react-bootstrap';

interface ExperimentMenuItemProps {
    id: string,
    name: string,
    max_iter: string,
    iter_no: string,
    resume: () => void,
    delete: () => void,
}

export const ExperimentListItem: React.StatelessComponent<ExperimentMenuItemProps> = (props) => {
    return (
        <Grid className="ExperimentListItem-container">
            <Row><strong>Id:</strong> {props.id}</Row>
            <Row><strong>Name:</strong> {props.name}</Row>
            <Row><strong>Max iterations:</strong> {props.max_iter}</Row>
            <Row><strong>Current iteration:</strong> {props.iter_no}</Row>
            <Row className="Experiment-button-container">
                <Button bsSize="small" bsStyle="warning" className="ExperimentStatus-button" disabled={false}
                        onClick={props.resume}>
                    RESUME
                </Button>
            </Row>
            <Row className="Experiment-button-container">
                <Button bsSize="small" bsStyle="danger" className="ExperimentStatus-button" disabled={false}
                        onClick={props.delete}>
                    DELETE
                </Button>
            </Row>
        </Grid>
    );
};
