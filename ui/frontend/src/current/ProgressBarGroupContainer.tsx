import * as React from "react";
import { SocketContextProps, withSocketContext } from "../common/SocketContext";
import { ProgressBar } from "react-bootstrap";

class ProgressBarGroupContainer extends React.Component<
  SocketContextProps,
  any
> {
  public constructor(props) {
    super(props);

    this.state = {
      evolutionProgress: -1,
      iterationProgress: -1,
      lastUpdate: "None",
      iterationName: "None"
    };
    const socket = this.props.socket;
    socket.on("exp_status", this.handleStatusUpdate);
    socket.on("exp_progress_update", this.handleProgressUpdate);
    socket.on("exp_progress_init", this.handleProgressUpdate);

    socket.emit("exp_progress_init_request");
  }

  public componentWillUnmount() {
    const socket = this.props.socket;
    socket.off("exp_status", this.handleStatusUpdate);
    socket.off("exp_progress_update", this.handleProgressUpdate);
    socket.off("exp_progress_init", this.handleProgressUpdate);
  }

  public render() {
    return (
      <div>
        <h2>Last update: {this.state.lastUpdate}</h2>
        <h2>Overall Evolution Progress</h2>
        <ProgressBar
          now={this.state.evolutionProgress}
          label={`${this.state.evolutionProgress}%`}
        />
        <h2>{`Current Iteration Progress: ${this.state.iterationName}`}</h2>
        <ProgressBar
          now={this.state.iterationProgress}
          label={`${this.state.iterationProgress}%`}
        />
      </div>
    );
  }

  private handleStatusUpdate = (message: any) => {
    this.setState({
      lastUpdate: message.update_timestamp
    });
  }

  private handleProgressUpdate = (message: any) => {
    this.setState({
      iterationProgress: message.iteration_progress,
      evolutionProgress: message.evolution_progress,
      iterationName: message.iteration_name
    });
  }

}

export default withSocketContext(ProgressBarGroupContainer);
