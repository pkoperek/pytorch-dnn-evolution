import * as React from 'react';
import {Graph} from "./Graph";
import * as d3 from 'd3';

import './graph.css';

export interface GraphProps {
    history: any
}

export interface State {
    containerId: string
}

export default class GraphContainer extends React.Component<GraphProps, State> {

    constructor(props) {
        super(props);
        const id = `graph-${(Math.random() * 100).toFixed(0)}`;
        this.state = {
            containerId: id,
        };
        this.initGraph(this.props);
    }

    public render() {
        return <Graph containerId={this.state.containerId}/>;
    }

    public componentWillUpdate(nextProps) {
        if (this.props.history !== nextProps.history) {
            this.initGraph(nextProps);
        }
    }

    public initGraph = (props) => {
        const parsed = typeof props.history === 'string' ? JSON.parse(props.history) : props.history;
        if (!!parsed.nodes.length && !!parsed.links.length) {
            d3.select(`#${this.state.containerId}`).selectAll('svg').remove();
        }

        const sorted = this.topSort(parsed.nodes, parsed.links);
        const width = 1000;
        const height = 80 * sorted.length;
        const nodes = sorted.map((list, index) => {
            return list.map((elem, idx) => {
                const xPosition = ((width / list.length) * (idx + 0.5));
                return {
                    name: elem.id,
                    x: xPosition,
                    y: 80 * index + 40,
                };
            });
        })
            .reduce((x, y) => x.concat(y), []);
        const links = parsed.links.map(d => {
            return {
                source: nodes.filter(nd => nd.name === d.source)[0],
                target: nodes.filter(nd => nd.name === d.target)[0]
            };
        });
        const graph = d3.select(`#${this.state.containerId}`).append("svg")
            .attr("width", width)
            .attr("height", height);

        const link = graph.append("g")
            .attr("class", "link")
            .selectAll("line")
            .data(links)
            .enter().append("line")
            .attr("x1", (d) => {
                return d.source.x;
            })
            .attr("y1", (d) => {
                return d.source.y;
            })
            .attr("x2", (d) => {
                return d.target.x;
            })
            .attr("y2", (d) => {
                return d.target.y;
            });
        const dragged = (d, index, elements) => {
            d.x = d3.event.x;
            d.y = d3.event.y;
            d3.select(elements[index]).attr("cx", d.x).attr("cy", d.y);
            d3.select(elements[index].nextSibling).attr("x", Number.parseInt(d.name, 10) > 9 ? d.x - 8 : d.x - 4).attr("y", d.y + 6);
            link.filter((l) => {
                return l.source === d;
            }).attr("x1", d.x).attr("y1", d.y);
            link.filter((l) => {
                return l.target === d;
            }).attr("x2", d.x).attr("y2", d.y);
        };
        const g = graph.append("g")
            .attr("class", "node")
            .selectAll("circle")
            .data(nodes)
            .enter()
            .append("g");
        g
            .append("circle")
            .attr("r", 20)
            .attr("cx", d => d.x)
            .attr("cy", d => d.y)
            .style("fill", 'darkblue')
            .call(d3.drag().on("drag", dragged));

        g
            .append("text").text(d => d.name)
            .attr("x", d => Number.parseInt(d.name, 10) > 9 ? d.x - 8 : d.x - 4)
            .attr("y", d => d.y + 6)
            .style("color", 'white');
    };
    private topSort = (nodes, links) => {
        const nodesCopy = JSON.parse(JSON.stringify(nodes));
        const findAllNotDependent = (nodeList, linksList) => {
            return nodeList.filter(node => linksList.filter(link => link.target === node.id).length === 0);
        };
        const removeAllWithGivenSource = (nodeList, linkList) => {
            return linkList.filter(link => nodeList.filter(node => (link.source === node.id)).length === 0);
        };
        const removeGivenNodes = (nodeList, list) => {
            return list.filter(elem => nodeList.filter(node => elem.id === node.id).length === 0);
        };
        const sorted = [];
        while (!this.containsAll(sorted, nodesCopy)) {
            const nonDependent = findAllNotDependent(nodes, links);
            sorted.push(nonDependent);
            nodes = removeGivenNodes(nonDependent, nodes);
            links = removeAllWithGivenSource(nonDependent, links);
        }
        return sorted;
    };
    private containsAll = (sorted, not) => {
        return sorted.reduce((x, y) => x.concat(y), []).length === not.length;
    }
}
