import * as React from 'react';
import {SocketContextProps, withSocketContext} from "../../common";
import GraphContainer from "./GraphContainer";

class GraphGroupContainer extends React.Component<SocketContextProps, any> {
    public constructor(props) {
        super(props);
        this.state = {
            mainHistory: {nodes: [], links: []},
            fpHistory: {nodes: [], links: []}
        };
        this.props.socket.on("main_history", this.handleMainHistory);
        this.props.socket.on("fp_history", this.handleFpHistory);
    }

    public render() {
        return (
            <div>
                <h1>Main history</h1>
                <GraphContainer history={this.state.mainHistory}/>
                <h1>FP history</h1>
                <GraphContainer history={this.state.fpHistory}/>
            </div>);
    }

    public componentWillUnmount() {
        this.props.socket.off("main_history", this.handleMainHistory);
        this.props.socket.off("fp_history", this.handleFpHistory);
    }

    private handleMainHistory = (message: any) => {
        this.setState({mainHistory: JSON.parse(message)});
    }

    private handleFpHistory = (message: any) => {
        this.setState({fpHistory: JSON.parse(message)});
    }
}


export default withSocketContext(GraphGroupContainer);
