import * as React from 'react';
import {MainChart} from "./MainChart";

interface Props {
    labels: string[],
    values: any
    title: string,
    append: boolean,
}

interface State {
    notUsed: string 
}

export class MainChartContainer extends React.Component<Props, State> {
    public render() {
        return (
            <MainChart 
                labels={this.props.labels} 
                values={this.props.values} 
                title={this.props.title}
                append={this.props.append}
            />
        );
    }
}
