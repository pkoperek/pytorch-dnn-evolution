import * as React from "react";
import {
  SocketContextProps,
  withSocketContext
} from "../../common";
import { MainChartContainer } from "./MainChartContainer";

class ChartGroupContainer extends React.Component<SocketContextProps, any> {
  private mainFitnessTitle: string = "Main population - fitness progress over time";
  private mainAccuracyTitle: string = "Main population - accuracy progress over time";
  private mainNoveltyTitle: string = "Main population - novelty progress over time";
  private fpFitnessTitle: string =
    "Fitness Predictors population - fitness progress over time";

  public constructor(props) {
    super(props);
    this.state = {
      fitness: {
        fp: { labels: [], values: [] },
        main: { labels: [], values: [] }
      },
      append: true
    };
    const socket = this.props.socket;
    socket.on("exp_statistics_update", this.handleStatisticsUpdate);
    socket.on("exp_statistics_init", this.handleStatisticsInit);
    socket.emit("exp_statistics_init_request");
  }

  public componentWillUnmount() {
    const socket = this.props.socket;
    socket.off("exp_statistics_update", this.handleStatisticsUpdate);
    socket.off("exp_statistics_init", this.handleStatisticsInit);
  }

  public render() {
    return (
      <div>
        <MainChartContainer
          labels={this.state.fitness.main.labels}
          values={this.state.fitness.main.values.fitness}
          title={this.mainFitnessTitle}
          append={this.state.append}
        />
        <MainChartContainer
          labels={this.state.fitness.main.labels}
          values={this.state.fitness.main.values.accuracy}
          title={this.mainAccuracyTitle}
          append={this.state.append}
        />
        <MainChartContainer
          labels={this.state.fitness.main.labels}
          values={this.state.fitness.main.values.novelty}
          title={this.mainNoveltyTitle}
          append={this.state.append}
        />
        <MainChartContainer
          labels={this.state.fitness.fp.labels}
          values={this.state.fitness.fp.values.fitness}
          title={this.fpFitnessTitle}
          append={this.state.append}
        />
      </div>
    );
  }

  private handleStatisticsUpdate = (message: any) => {
    console.log("handleStatisticsUpdate");
    console.log(message);
    this.setState({
      fitness: message,
      append: true
    });
  };

  private handleStatisticsInit = (message: any) => {
    console.log("handleStatisticsInit");
    console.log(message);
    this.setState({
      fitness: message,
      append: false
    });
  };
}

export default withSocketContext(ChartGroupContainer);
