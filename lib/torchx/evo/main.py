import logging
import sys
import os
import json
import warnings


warnings.filterwarnings(
    "ignore",
    message="torch.distributed.reduce_op is deprecated, please use torch.distributed.ReduceOp instead")
logging.basicConfig(
    level=logging.INFO,
    format='[%(asctime)s: %(levelname)s/%(name)s] %(message)s',
    handlers=[logging.StreamHandler(sys.stdout)]
)

logger = logging.getLogger(__name__)

from torchxui.coevo_state import CoEvolutionState
from torchxui.json import setup_json_serialization
from ui.tests.test_unit import (
    MockDNNEvoZookeeperClient,
    MockDBSession
)


def load_configuration():
    config_path = "config.json"
    if len(sys.argv) >= 2:
        config_path = os.path.join(sys.argv[1], config_path)
    with open(config_path, "r") as config_file:
        return json.load(config_file)


def emulate_front_configuration(config):
    config['evo-config']['novelty'] = str(config['evo-config']['novelty'])
    config['evo-config'] = json.dumps(config['evo-config'])


def main():
    setup_json_serialization()
    configuration = load_configuration()
    logger.info(f"Configuration:\n\n{json.dumps(configuration, indent=2)}\n\n")
    emulate_front_configuration(configuration)
    zk_client = MockDNNEvoZookeeperClient()
    db_session = MockDBSession()
    coevolution_state = CoEvolutionState(configuration, db_session, zk_client)
    coevolution_state()


if __name__ == '__main__':
    main()
