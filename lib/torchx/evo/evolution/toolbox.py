import logging
import random
import numpy as np
from dataclasses import dataclass
from typing import Tuple, Optional, List
from inspect import isclass

from deap import base, creator, tools

from torchx.evo.datasets import retrieve_dataset
from torchx.evo.evolution.configuration import DNNEvoConfiguration, NoveltyComparisonMethod, NoveltyArchiveType
from torchx.evo.genotype import NUMBER_OF_FIELDS, NUMBER_OF_EXTRA_FIELDS
from torchx.evo.archive.pyod_archive import PyODArchive
from torchx.evo.archive.abod_archive import AbodArchive
from torchx.evo.archive.cblof_archive import CblofArchive
from torchx.evo.archive.hbos_archive import HbosArchive
from torchx.evo.archive.knn_archive import KnnArchive
from torchx.evo.archive.loci_archive import LociArchive
from torchx.evo.archive.lof_archive import LofArchive
from torchx.evo.archive.sos_archive import SosArchive

logger = logging.getLogger('evolution')


def signature(trainers_population):
    """
    Creates a hash for the population of trainers - in reality a list of lists.
    Order of trainers doesn't matter - so we sort the trainers within the
    population.
    """
    as_tuples = tuple(sorted([tuple(i) for i in trainers_population]))
    return hash(as_tuples)


def mut_uniform(individual, indpb):
    """This function applies a uniform mutation on the input individual. This
    mutation expects a :term:`sequence` individual composed of real valued
    attributes. The *indpb* argument is the probability of each attribute to be
    mutated.

    :param individual: Individual to be mutated.
    :param indpb: Independent probability for each attribute to be mutated.
    :returns: A tuple of one individual.

    This function uses the :func:`~random.random` functions from the python
    base :mod:`random` module.
    """
    for i in range(len(individual)):
        if random.random() < indpb:
            individual[i] = random.random()

    return individual,


class TrainerAwareFitness(base.Fitness):

    def __init__(self, values=(), train_sign=None):
        super().__init__(values)
        self._train_sign = train_sign

    @property
    def train_sign(self) -> Optional[None]:
        """Signature of the trainers used to generate the fitness values"""
        return self._train_sign

    @train_sign.setter
    def train_sign(self, value):
        self._train_sign = value

    @train_sign.deleter
    def train_sign(self):
        self._train_sign = None

    def __hash__(self):
        return hash((self.wvalues, self.train_sign))

    def _raise_if_different_trainers(self, other):
        if other.train_sign != self.train_sign:
            raise ValueError(
                "Trying to compare fitness values generated with use of different trainers! "
                f"Left: {other.train_sign} Right: {self.train_sign}"
            )

    def __le__(self, other):
        self._raise_if_different_trainers(other)
        return self.wvalues <= other.wvalues

    def __lt__(self, other):
        self._raise_if_different_trainers(other)
        return self.wvalues < other.wvalues

    def __eq__(self, other):
        self._raise_if_different_trainers(other)
        return self.wvalues == other.wvalues

    def __deepcopy__(self, memo):
        """Replace the basic deepcopy function with a faster one.
        It assumes that the elements in the :attr:`values` tuple are
        immutable and the fitness does not contain any other object
        than :attr:`values` and :attr:`weights`.
        """
        cp = self.__class__()
        cp.wvalues = self.wvalues
        cp.train_sign = self.train_sign
        return cp

    def __str__(self):
        """Return the values and trainer signature of the Fitness object."""
        values = self.values if self.valid else tuple()
        return f"{values} train_sign: {self.train_sign}"

    def __repr__(self):
        """Return the Python code to build a copy of the object."""
        mod = self.__module__
        cls = self.__class__.__name__
        values = self.values if self.valid else tuple()
        return f"{mod}.{cls}({values!r}, {self.train_sign!r})"


class Individual(list):
    fitness: TrainerAwareFitness
    accuracy: float
    accuracy_per_class: np.ndarray
    novelty: float
    eval_time: float


Population = List[Individual]


@dataclass
class ToolboxBuilder:
    configuration: DNNEvoConfiguration

    def build(self) -> Tuple[base.Toolbox, base.Toolbox]:
        return self._build_main_toolbox(), self._build_fp_toolbox()

    def _build_fp_toolbox(self):
        """
        Fitness Predictor population configuration
        """
        # fitness predictors minimize the value - it is the difference to the
        # trainer fitness on full dataset
        creator.create("FPFitness", TrainerAwareFitness, weights=(-1.0,))
        creator.create("FPIndividual", Individual, fitness=creator.FPFitness, accuracy=0., eval_time=0.)

        config = self.configuration.fp_train_config
        dataset = retrieve_dataset(config.dataset)
        max_sample_id = dataset.samples_count - 1

        fp_toolbox = base.Toolbox()
        fp_toolbox.register("map", lambda f, *i: list(map(f, *i)))

        def create_logbook():
            logbook = tools.Logbook()
            logbook.log_header = False
            logbook.header = ("gen", "max", "min", "avg", "std")
            return logbook
        fp_toolbox.register("create_logbooks",
                            lambda: {
                                "fitness": create_logbook()
                            })

        def logbooks_record(logbooks, gen, records):
            for key, record in records.items():
                logbooks[key].record(gen=gen, **record)
        fp_toolbox.register("logbooks_record", logbooks_record)

        def create_statistics(key):
            stats = tools.Statistics(key=key)
            stats.register("avg", np.mean)
            stats.register("std", np.std)
            stats.register("min", np.min)
            stats.register("max", np.max)
            return stats

        fp_toolbox.register("create_statistics",
                            lambda: {
                                "fitness": create_statistics(lambda ind: ind.fitness.values)
                            })
        fp_toolbox.register("compile_stats",
                            lambda stats_dict, population:
                            dict(map(
                                lambda pair:
                                (pair[0], pair[1].compile(population)), stats_dict.items())))

        fp_toolbox.register("attr_int", random.randint, 0, max_sample_id)
        fp_toolbox.register("individual",
                            tools.initRepeat,
                            creator.FPIndividual,
                            fp_toolbox.attr_int,
                            n=self.configuration.fp_individual_size)
        fp_toolbox.register("population", tools.initRepeat,
                            list,
                            fp_toolbox.individual,
                            self.configuration.fp_population_size)
        fp_toolbox.register("mate", tools.cxTwoPoint)
        fp_toolbox.register("mutate", tools.mutUniformInt,
                            low=0, up=max_sample_id,
                            indpb=self.configuration.fp_attribute_mutation_probability)
        fp_toolbox.register("select", tools.selTournament, tournsize=2)
        fp_toolbox.register("ind_mut_probability",
                            lambda: self.configuration.fp_individual_mutation_probability)
        fp_toolbox.register("cx_probability",
                            lambda:
                            self.configuration.fp_crossover_probability)

        fp_toolbox.register("max_gene_value", lambda: max_sample_id)

        def update_individual_accuracy(individual, accuracy_result):
            individual.accuracy = accuracy_result

        fp_toolbox.register("update_individual_accuracy", update_individual_accuracy)
        fp_toolbox.register("is_novelty_calculation_enabled", lambda: False)
        fp_toolbox.register("novelty_should_use_genotype", lambda: False)
        fp_toolbox.register("archive", lambda entries: None)
        fp_toolbox.register("calculate_individual_fitness", lambda individual, iteration_no: (individual.accuracy, ))

        return fp_toolbox

    def _build_main_toolbox(self):
        """
        Main population configuration

        Main fitness is looking for highest value - fitness is recognition
        accuracy on test set (so 100% is max)
        """
        creator.create("MainFitness", TrainerAwareFitness, weights=(1.0,))
        if self.configuration.novelty_genotype:
            creator.create("MainIndividual", Individual, fitness=creator.MainFitness,
                           accuracy=0., novelty=0., eval_time=0.)
        else:
            creator.create("MainIndividual", Individual, fitness=creator.MainFitness,
                           accuracy=0., novelty=0., accuracy_per_class=None, eval_time=0.)

        individual_size = self.configuration.main_individual_size
        genotype_size = NUMBER_OF_EXTRA_FIELDS + individual_size * NUMBER_OF_FIELDS

        main_toolbox = base.Toolbox()
        main_toolbox.register("map", lambda f, *i: list(map(f, *i)))

        def create_logbook():
            logbook = tools.Logbook()
            logbook.log_header = False
            logbook.header = ("gen", "max", "min", "avg", "std")
            return logbook
        main_toolbox.register("create_logbooks",
                              lambda: {
                                  "fitness": create_logbook(),
                                  "accuracy": create_logbook(),
                                  "novelty": create_logbook()
                              })

        def logbooks_record(logbooks, gen, records):
            for key, record in records.items():
                logbooks[key].record(gen=gen, **record)
        main_toolbox.register("logbooks_record", logbooks_record)

        def create_statistics(key):
            stats = tools.Statistics(key=key)
            stats.register("avg", np.mean)
            stats.register("std", np.std)
            stats.register("min", np.min)
            stats.register("max", np.max)
            return stats

        main_toolbox.register("create_statistics",
                              lambda: {
                                  "fitness": create_statistics(lambda ind: ind.fitness.values),
                                  "accuracy": create_statistics(lambda ind: ind.accuracy),
                                  "novelty": create_statistics(lambda ind: ind.novelty),
                              })
        main_toolbox.register("compile_stats",
                              lambda stats_dict, population:
                              dict(map(
                                lambda pair:
                                (pair[0], pair[1].compile(population)), stats_dict.items())))

        main_toolbox.register("attr_float", random.random)
        main_toolbox.register("individual",
                              tools.initRepeat,
                              creator.MainIndividual,
                              main_toolbox.attr_float,
                              n=genotype_size)
        main_toolbox.register("population", tools.initRepeat, list,
                              main_toolbox.individual,
                              self.configuration.main_population_size)
        main_toolbox.register("mate", tools.cxTwoPoint)
        main_toolbox.register("mutate", mut_uniform,
                              indpb=
                              self.configuration.main_attribute_mutation_probability)
        main_toolbox.register("select",
                              tools.selTournament, tournsize=2)
        main_toolbox.register("ind_mut_probability",
                              lambda:
                              self.configuration.main_individual_mutation_probability)
        main_toolbox.register("cx_probability",
                              lambda:
                              self.configuration.main_crossover_probability)

        main_toolbox.register("max_gene_value", lambda: 1.0)

        if not self.configuration.is_novelty_enabled() or self.configuration.novelty_genotype:
            def update_individual_accuracy(individual, accuracy_result):
                individual.accuracy = accuracy_result.accuracy
        else:
            def update_individual_accuracy(individual, accuracy_result):
                individual.accuracy = accuracy_result.accuracy
                individual.accuracy_per_class = accuracy_result.accuracy_per_class

        if self.configuration.novelty_compare_to == NoveltyComparisonMethod.PREV_GENS:
            def get_novelty(novelty_dataset, archive, iteration_no):
                if iteration_no == 0:
                    return np.zeros(len(novelty_dataset))
                return archive.get_novelty(novelty_dataset) * 100
        else:
            def get_novelty(novelty_dataset, archive, iteration_no):
                return archive.get_novelty(novelty_dataset) * 100

        main_toolbox.register("update_individual_accuracy", update_individual_accuracy)
        main_toolbox.register("is_novelty_calculation_enabled", lambda: self.configuration.is_novelty_enabled())
        main_toolbox.register("novelty_should_use_genotype", lambda: self.configuration.novelty_genotype)

        def archive(entries):
            archive_type = self.configuration.novelty_archive_type
            compare_to = self.configuration.novelty_compare_to

            if archive_type == NoveltyArchiveType.ABOD:
                archive = AbodArchive(compare_to)
            elif archive_type == NoveltyArchiveType.CBLOF:
                archive = CblofArchive(compare_to)
            elif archive_type == NoveltyArchiveType.HBOS:
                archive = HbosArchive(compare_to)
            elif archive_type == NoveltyArchiveType.KNN:
                archive = KnnArchive(compare_to)
            elif archive_type == NoveltyArchiveType.LOCI:
                archive = LociArchive(compare_to)
            elif archive_type == NoveltyArchiveType.LOF:
                archive = LofArchive(compare_to)
            elif archive_type == NoveltyArchiveType.SOS:
                archive = SosArchive(compare_to)
            else:
                raise ValueError(f"Unknown archive type: {archive_type}")

            if entries is not None and len(entries) > 0:
                entries = np.array(entries)
                entries = entries.reshape(entries.shape[0], -1)
                archive.insert(entries)
            return archive

        main_toolbox.register("archive", archive)
        main_toolbox.register("calculate_novelty", get_novelty)

        def get_novelty_weight(iteration_no):
            return self.configuration.novelty / 100 * (1 - self.configuration.novelty_fading_factor) ** iteration_no
        
        def get_accuracy_weight():
            return 1 - self.configuration.novelty / 100
        
        def get_fitness_rescaling_factor(iteration_no):
            return 1 / (get_accuracy_weight() + get_novelty_weight(iteration_no))

        main_toolbox.register("calculate_individual_fitness",
                              lambda individual, iteration_no: (
                                  (
                                      individual.novelty * get_novelty_weight(iteration_no)
                                      + individual.accuracy * get_accuracy_weight()
                                  )
                                  * get_fitness_rescaling_factor(iteration_no),
                              )
        )

        return main_toolbox
