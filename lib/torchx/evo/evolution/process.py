import logging
import random
import json
from collections import deque
from dataclasses import dataclass, field, InitVar
from typing import Deque, Callable, TypeVar, Generic, List, Dict, Optional

import numpy as np
from deap import base, tools

from torchx.evo.evolution.configuration import ConfigurationDict
from torchx.evo.evolution.toolbox import (
    Individual,
    Population,
    signature,
)
from torchx.evo.utils.iteration_tracker import StatefulIterationTracker
from torchx.evo.utils.promises import AvgCompositeValuePromise, ValuePromise
from torchx.evo.archive.pyod_archive import PyODArchive

logger = logging.getLogger('evolution')

Trainer = TypeVar('Trainer')
EvaluateFunc = Callable[[Individual, Trainer, ConfigurationDict], ValuePromise[float]]


@dataclass
class Evaluator(Generic[Trainer]):
    evaluate_with_trainer: EvaluateFunc[Trainer]
    evaluation_parameters: ConfigurationDict
    max_trainers: InitVar[int] = 1

    trainers: Deque[Trainer] = field(init=False)

    def __post_init__(self, max_trainers: int):
        self.trainers = deque(maxlen=max_trainers)

    def evaluate(self, individual: Individual) -> ValuePromise[float]:
        if not self.trainers:
            raise ValueError("Evaluator: no trainers have been added! Can't evaluate!")

        return AvgCompositeValuePromise([
            self.evaluate_with_trainer(
                individual,
                trainer,
                self.evaluation_parameters
            )
            for trainer in self.trainers
        ])


class PopulationEvolution(Generic[Trainer]):
    population_name: str
    toolbox: base.Toolbox
    evaluator: Evaluator[Trainer]
    archive: PyODArchive
    population: Population
    iteration: StatefulIterationTracker
    history: tools.History
    logbooks: Dict[str, tools.Logbook]
    stats: Dict[str, tools.Statistics]
    latest_archive_entries: Optional[np.ndarray] = None
    logbook_diversity: tools.Logbook

    def __init__(
            self,
            population_name: str,
            toolbox: base.Toolbox,
            evaluator: Evaluator[Trainer],
            individuals: List,
            archive_entries: List,
            iteration_no: int,
            current_target: int = None
    ):
        self.population_name = population_name
        self.toolbox = toolbox
        self.evaluator = evaluator
        self.archive = self.toolbox.archive(archive_entries)

        logger.debug('Loading population ')
        self.population = self.toolbox.population()
        logger.debug(f'Population created: {self.population}')
        if individuals is not None:
            logger.debug('appending old population data ' + str(individuals))
            for (population_individual, stored_individual) in zip(self.population, individuals):
                population_individual.clear()
                population_individual.extend(json.loads(stored_individual.genotype))

        self._log = logger.getChild(self.population_name)

        # in the first iteration we have to evaluate most probably x2 population length
        initial_target = current_target if current_target is not None else len(self.population) * 2
        self.iteration = StatefulIterationTracker(log=self._log,
                                                  initial_no=iteration_no,
                                                  initial_target=initial_target,
                                                  name=f"Evolution of {self.population_name.split('_')[-1]} population")
        logger.debug('StatefulIterationTracker loaded')

        self.logbooks = self.toolbox.create_logbooks()
        logger.debug('Logbooks loaded')

        self.stats = self.toolbox.create_statistics()
        logger.debug('Statistics loaded')

        self.history = tools.History()
        self.toolbox.decorate("mate", self.history.decorator)
        self.toolbox.decorate("mutate", self.history.decorator)
        self.history.update(self.population)
        logger.debug('PopulationEvolution created')

        self.logbook_diversity = tools.Logbook()
        self.logbook_diversity.log_header = False
        self.logbook_diversity.header = ("gen", "diversity")

    def statistics_history(self):
        """
        Returns the statistics history as a dictionary of list of lists of values. The order
        is the same as in logbooks headers
        """
        values = dict(map(
            lambda pair:
            (pair[0], [list(history)
                       for history
                       in pair[1].select(*pair[1].header)])
            , self.logbooks.items()))
        labels = list(next(iter(self.logbooks.values())).header)

        return {
            'values': values,
            'labels': labels
        }

    def diversity_history(self):
        values = [
            list(history)
            for history
            in self.logbook_diversity.select(*self.logbook_diversity.header)
        ]
        labels = list(self.logbook_diversity.header)

        return {
            'values': values,
            'labels': labels
        }

    def last_fitness_update(self):
        labels = list(next(iter(self.logbooks.values())).header)

        # every element is wrapped in a list to make the format look
        # the same as in the statistics_history() call
        # this makes implementing client using this data easier
        values = dict(map(
            lambda pair:
            (pair[0], [pair[1][-1][label] for label in labels])
            , self.logbooks.items()))

        return {
            'values': values,
            'labels': labels,
        }

    def max_fitness(self, last_n):
        return self.logbooks["fitness"].select("max")[-last_n:]

    def max_accuracy(self, last_n):
        return self.logbooks["accuracy"].select("max")[-last_n:]

    def best(self):
        # if current iteration number == 0 evolve() was not called yet - we need to pick random
        if self.iteration.current_no == 0:
            idx = random.randint(0, len(self.population) - 1)
            best_individual = self.population[idx]
        else:
            best_individual = tools.selBest(self.population, 1)[0]

        best_clone = self.toolbox.clone(best_individual)
        return best_clone

    def _calculate_diversity(self):
        return 200. * np.mean(np.std(self.population, axis=0)) / self.toolbox.max_gene_value()

    def _record_diversity(self, gen):
        self.logbook_diversity.record(gen=gen, diversity=self._calculate_diversity())

    def apply_crossovers(self, individuals):
        for i in range(1, len(individuals), 2):
            if random.random() < self.toolbox.cx_probability():
                individuals[i - 1], individuals[i] = self.toolbox.mate(individuals[i - 1], individuals[i])
                del individuals[i - 1].fitness.values
                del individuals[i].fitness.values
                del individuals[i - 1].fitness.train_sign
                del individuals[i].fitness.train_sign

    def apply_mutations(self, individuals):
        for i in range(len(individuals)):
            if random.random() < self.toolbox.ind_mut_probability():
                individuals[i], = self.toolbox.mutate(individuals[i])
                del individuals[i].fitness.values
                del individuals[i].fitness.train_sign

    def evaluate_accuracy(self, individuals, iteration_section):
        train_sign = signature(self.evaluator.trainers)
        individuals_to_evaluate = [
            ind
            for ind in individuals
            if not ind.fitness.valid or ind.fitness.train_sign != train_sign
        ]
        iteration_section.set_target(len(individuals_to_evaluate))

        if not individuals_to_evaluate:
            self._log.debug("All individuals have valid fitnesses, skipping accuracy evaluation")
        else:
            self._log.debug(f"Using trainers: {self.evaluator.trainers} Signature: {train_sign}")
            promises = self.toolbox.map(self.evaluator.evaluate, individuals_to_evaluate)

            def retrieve_value_and_time(promise):
                value = promise.get_value()
                time = promise.get_total_cpu_time_s()
                iteration_section.tick()
                return value, time

            retrieved_values = self.toolbox.map(retrieve_value_and_time, promises)
            for ind, (accuracy_result, eval_time) in zip(individuals_to_evaluate, retrieved_values):
                self.toolbox.update_individual_accuracy(ind, accuracy_result)
                ind.eval_time = eval_time
                ind.fitness.train_sign = train_sign
                self._log.debug(f"Evaluated accuracy for: {ind}")

    def evaluate_novelty(self, individuals):
        if self.toolbox.novelty_should_use_genotype():
            novelty_dataset = np.array(individuals)
        else:
            novelty_dataset = np.array([ind.accuracy_per_class for ind in individuals])

        novelty_results = self.toolbox.calculate_novelty(
            novelty_dataset,
            self.archive,
            self.iteration.current_no
        )
        self.archive.insert(novelty_dataset)
        self.latest_archive_entries = novelty_dataset

        for ind, novelty in zip(individuals, novelty_results):
            ind.novelty = novelty
            self._log.debug(f"Evaluated novelty for: {ind}")

    def evaluate(self, individuals, iteration_section):
        self.evaluate_accuracy(individuals, iteration_section)
        if self.toolbox.is_novelty_calculation_enabled():
            self.evaluate_novelty(individuals)
        for ind in individuals:
            ind.fitness.values = self.toolbox.calculate_individual_fitness(ind, self.iteration.current_no)
            self._log.debug(f"Fitness calculated for: {ind} Fitness: {ind.fitness}")

    def evolve(self):
        """Implementation based on
        https://github.com/DEAP/deap/blob/master/deap/algorithms.py ->
        eaSimple"""

        with self.iteration.enter() as iteration_section:
            if self.iteration.current_no == 0:
                all_individuals = self.population
            else:
                offspring: Population = [self.toolbox.clone(ind) for ind in self.population]
                self.apply_crossovers(offspring)
                self.apply_mutations(offspring)

                all_individuals = self.population + offspring

            self.evaluate(all_individuals, iteration_section)

            self.population = self.toolbox.select(all_individuals, len(self.population))

            self._log.debug(f"Population length after selection: {len(self.population)}")
            self._log.debug(f"First fitness: {self.population[0].fitness}")
            records = self.toolbox.compile_stats(self.stats, self.population)
            self.toolbox.logbooks_record(self.logbooks, self.iteration.current_no, records)
            self._record_diversity(self.iteration.current_no)
