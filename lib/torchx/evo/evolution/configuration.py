import logging
from abc import ABC
from enum import Enum
from dataclasses import dataclass, field, asdict, fields, is_dataclass
from typing import Dict, TypeVar, Type, Union

logger = logging.getLogger('evolution')

K = TypeVar('K', bound='Configuration')

ConfigurationDict = Dict[str, Union[str, int, float, bool, 'ConfigurationDict']]

@dataclass
class Configuration(ABC):
    def to_dict(self) -> ConfigurationDict:
        return asdict(self)

    @classmethod
    def from_dict(cls: Type[K], source_dict: ConfigurationDict) -> K:
        def dataclass_from_dict(subclass, src_dict):
            if is_dataclass(subclass):
                fieldtypes = {f.name:f.type for f in fields(subclass)}
                return subclass(**{f:dataclass_from_dict(fieldtypes[f], src_dict[f]) for f in src_dict})
            else:
                return subclass(src_dict)

        config = dataclass_from_dict(cls, source_dict)
        logger.info(f"Turned: {source_dict} into {config}")

        return config


@dataclass
class DNNTrainConfiguration(Configuration):
    optimizer: str = 'sgd'
    rng_seed: int = -1
    batch_size: int = 128
    learning_rate: float = 0.1
    momentum: float = 0.0
    dataset: str = 'mnist'
    dense_size_multiplier: int = 500
    convolution_size_multiplier: int = 5
    train_iterations: int = 12
    calculate_accuracy_per_class: bool = False


class NoveltyArchiveType(Enum):
    ABOD = "abod"
    CBLOF = "cblof"
    HBOS = "hbos"
    KNN = "knn"
    LOCI = "loci"
    LOF = "lof"
    SOS = "sos"


class NoveltyComparisonMethod(Enum):
    PREV_GENS = "prev gens"
    ALL = "all"
    ALL_BESIDES_SELF = "all besides self"


@dataclass
class DNNEvoConfiguration(Configuration):
    main_train_config: DNNTrainConfiguration = field(default_factory=DNNTrainConfiguration)
    fp_train_config: DNNTrainConfiguration = field(default_factory=DNNTrainConfiguration)

    stale_iterations_cnt: int = 5
    """
    In how many iterations we need to have the same max value
    so new trainer search is executed.
    """

    main_individual_size: int = 64
    main_individual_mutation_probability: float = 0.02
    main_attribute_mutation_probability: float = 0.02
    main_population_size: int = 32
    main_crossover_probability: float = 0.50

    fp_individual_size: int = 1000
    fp_individual_mutation_probability: float = 0.02
    fp_attribute_mutation_probability: float = 0.02
    fp_population_size: int = 8
    fp_crossover_probability: float = 0.50

    novelty: float = 0.
    novelty_genotype: bool = False
    novelty_fading_factor: float = 0
    novelty_archive_type: NoveltyArchiveType = NoveltyArchiveType.KNN
    novelty_compare_to: NoveltyComparisonMethod = NoveltyComparisonMethod.ALL_BESIDES_SELF

    archive_threshold: float = 0.01
    archive_neighbours_count: int = 3

    def is_novelty_enabled(self):
        return self.novelty != 0.

    def set_main_novelty_calculation(self):
        self.main_train_config.calculate_accuracy_per_class = self.novelty > 0 and not self.novelty_genotype

    def __setattr__(self, name, value):
        super(DNNEvoConfiguration, self).__setattr__(name, value)
        if name == "novelty" or name == "novelty_genotype":
            self.set_main_novelty_calculation()

    def __post_init__(self):
        self.set_main_novelty_calculation()
