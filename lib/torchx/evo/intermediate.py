from torchx.evo.genotype import Gene


def genotype_to_layers(genotype):
    """
        Returns a list of layers where id in the list is the id of the gene.
        We start the ids from 1 - not 0, because we always want to add an entry
        layer with 0,0 and exit layer with 1,1.

        This function assigns ids to layers in an linear way. Given that we
        enhance the genotype artificially with enter/exit genes this means that
        entry layer has id 0 and output layer has highest id among all other
        layers.
    """
    layer_id = 0
    genes_cnt = genotype.size
    previous_type = Gene.INPUT
    current_layer = None
    genes_to_layers = []

    for i in range(genes_cnt):
        gene = genotype.gene(i)
        layer_type = gene.layer_type

        if current_layer is None:
            current_layer = Layer(layer_id, layer_type)
            layer_id += 1

        if gene.is_input():
            if previous_type == Gene.OUTPUT:
                current_layer = Layer(layer_id, layer_type)
                layer_id += 1

            previous_type = Gene.INPUT
            current_layer.add_input(gene.x, gene.y)
        else:
            previous_type = Gene.OUTPUT
            current_layer.add_output(gene.x, gene.y)

        current_layer.add_gene_id(i)
        current_layer.add_extra(gene.extra)
        genes_to_layers.append(current_layer)

    genes_to_layers[0].mark_entry_layer()
    genes_to_layers[-1].mark_exit_layer()

    return genes_to_layers


class Layer(object):

    DENSE = 'dense'
    CONVOLUTION = 'convolution'

    types = [
        DENSE,
        CONVOLUTION
    ]

    def __init__(self, lid, layer_type):
        self.__inputs = []
        self.__outputs = []
        self.__extras = []
        self.__id = lid
        self.__neighbors = []
        self.__gene_ids = []
        self.__dependencies = []
        self.__is_entry = False
        self.__is_exit = False
        self.__raw_layer_type = layer_type
        self.__layer_type = self.__calculate_type()

    def __calculate_type(self):
        if self.__raw_layer_type == 1.0:
            return Layer.types[-1]

        step = 1.0 / len(Layer.types)

        bucket = 0
        bucket_start = 0.0

        while bucket_start + step <= self.__raw_layer_type:
            bucket += 1
            bucket_start += step

        return Layer.types[bucket]

    def __eq__(self, other):
        return self.__id == other.__id

    def __hash__(self):
        return self.__id

    @property
    def layer_type(self):
        return self.__layer_type

    @property
    def dependencies(self):
        return self.__dependencies

    @property
    def neighbors(self):
        return self.__neighbors

    @property
    def id(self):
        return self.__id

    @property
    def outputs(self):
        return self.__outputs

    @property
    def extras(self):
        return self.__extras

    @property
    def inputs(self):
        return self.__inputs

    @property
    def gene_ids(self):
        return self.__gene_ids

    def add_input(self, x, y):
        as_tuple = (x, y)
        self.__inputs.append(as_tuple)

    def add_output(self, x, y):
        as_tuple = (x, y)
        self.__outputs.append(as_tuple)

    def add_extra(self, extra):
        self.__extras.append(extra)

    def add_neighbor(self, neighbor, *neighbors):
        """
            This is a "forward" connection
        """
        to_add = [neighbor] + list(neighbors)
        for neighbor in to_add:
            if self.id != neighbor.id\
                    and neighbor not in self.__neighbors:
                self.__neighbors.append(neighbor)

    def remove_neighbor(self, neighbor):
        self.__neighbors.remove(neighbor)

    def add_gene_id(self, gene_id):
        self.__gene_ids.append(gene_id)

    def add_dependency(self, dependency):
        """
            This is a "backward" connection
        """
        if dependency not in self.__dependencies:
            self.__dependencies.append(dependency)

    def __str__(self):
        return "Layer(id={})".format(self.__id)

    def __repr__(self):
        return self.__str__()

    def mark_entry_layer(self):
        self.__is_entry = True

    def mark_exit_layer(self):
        self.__is_exit = True

    def is_entry_layer(self):
        return self.__is_entry

    def is_exit_layer(self):
        return self.__is_exit

    def clear_neighbors(self):
        self.__neighbors.clear()
