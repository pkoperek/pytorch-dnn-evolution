import json
import numpy as np

from typing import Union
from dataclasses import dataclass


@dataclass(frozen=True)
class EvaluationResult:
    accuracy: float = 0.0

    def to_json(self):
        return json.dumps(self)

    @classmethod
    def from_json(cls, json_str):
        json_dict = json.loads(json_str)
        return cls(**json_dict)

    def __add__(self, other: Union[int, float, 'EvaluationResult']) -> 'EvaluationResult':
        if isinstance(other, EvaluationResult):
            return EvaluationResult(accuracy=self.accuracy + other.accuracy)
        return EvaluationResult(accuracy=self.accuracy + other)

    def __radd__(self, other: Union[int, float, 'EvaluationResult']) -> 'EvaluationResult':
        return self.__add__(other)

    def __sub__(self, other: Union[int, float, 'EvaluationResult']) -> 'EvaluationResult':
        if isinstance(other, EvaluationResult):
            return EvaluationResult(accuracy=self.accuracy - other.accuracy)
        return EvaluationResult(accuracy=self.accuracy - other)

    def __rsub__(self, other: Union[int, float, 'EvaluationResult']) -> 'EvaluationResult':
        return self.__sub__(other)

    def __mul__(self, other: Union[int, float, 'EvaluationResult']) -> 'EvaluationResult':
        if isinstance(other, EvaluationResult):
            return EvaluationResult(accuracy=self.accuracy * other.accuracy)
        return EvaluationResult(accuracy=self.accuracy * other)

    def __rmul__(self, other: Union[int, float, 'EvaluationResult']) -> 'EvaluationResult':
        return self.__mul__(other)

    def __truediv__(self, other: Union[int, float, 'EvaluationResult']) -> 'EvaluationResult':
        if isinstance(other, EvaluationResult):
            return EvaluationResult(accuracy=self.accuracy / other.accuracy)
        return EvaluationResult(accuracy=self.accuracy / other)

    def __rtruediv__(self, other: Union[int, float, 'EvaluationResult']) -> 'EvaluationResult':
        return self.__truediv__(other)


@dataclass(frozen=True)
class NoveltyEvaluationResult(EvaluationResult):
    accuracy_per_class: np.ndarray = np.zeros(10)

    def __add__(self, other: 'NoveltyEvaluationResult') -> 'NoveltyEvaluationResult':
        return NoveltyEvaluationResult(
            accuracy=self.accuracy + other.accuracy,
            accuracy_per_class=self.accuracy_per_class + other.accuracy_per_class
        )

    def __radd__(self, other: 'NoveltyEvaluationResult') -> 'NoveltyEvaluationResult':
        return self.__add__(other)

    def __sub__(self, other: 'NoveltyEvaluationResult') -> 'NoveltyEvaluationResult':
        return NoveltyEvaluationResult(
            accuracy=self.accuracy - other.accuracy,
            accuracy_per_class=self.accuracy_per_class - other.accuracy_per_class
        )

    def __rsub__(self, other: 'NoveltyEvaluationResult') -> 'NoveltyEvaluationResult':
        return self.__sub__(other)

    def __mul__(self, other: Union[int, float]) -> 'NoveltyEvaluationResult':
        return NoveltyEvaluationResult(
            accuracy=self.accuracy * other,
            accuracy_per_class=self.accuracy_per_class * other
        )

    def __rmul__(self, other: Union[int, float]) -> 'NoveltyEvaluationResult':
        return self.__mul__(other)

    def __truediv__(self, other: Union[int, float]) -> 'NoveltyEvaluationResult':
        return NoveltyEvaluationResult(
            accuracy=self.accuracy / other,
            accuracy_per_class=self.accuracy_per_class / other
        )

    def __rtruediv__(self, other: Union[int, float]) -> 'NoveltyEvaluationResult':
        return self.__truediv__(other)
