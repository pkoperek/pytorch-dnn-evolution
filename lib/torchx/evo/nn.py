import math

from torch.autograd import Variable
from torchx.evo.genotype import Genotype, genes_adjacency_matrix
from torchx.evo.intermediate import genotype_to_layers, Layer
from torchx.evo.evaluation_result import EvaluationResult, NoveltyEvaluationResult
from torch.nn.modules.module import _addindent
from torch.utils.data.sampler import SubsetRandomSampler
from torchx.evo.datasets import retrieve_dataset
from functools import reduce
from collections import defaultdict

import os
import logging
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np


TORCH_NUM_THREADS = int(os.environ.get('TORCH_NUM_THREADS', '1'))
torch.set_num_threads(TORCH_NUM_THREADS)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('dnnevo')


class ProgressTracker(object):

    def __init__(self):
        self.__target_value = 0

    def set_target_value(self, target_value):
        self.__target_value = target_value

    @property
    def target_value(self):
        return self.__target_value

    def update_progress(self, updated_progress):
        raise NotImplementedError("update_progress not implemented!")


class IgnoringProgressTracker(ProgressTracker):

    def update_progress(self, update_progress):
        pass


class GraphModule(nn.Module):

    def __init__(self, exit_node, parameters):
        super(GraphModule, self).__init__()

        self.__parameters = parameters
        self.__exit_node = exit_node
        self.log_softmax = nn.LogSoftmax(dim=1)

    def build(self):
        self.__exit_node.build()

    @property
    def exit_node(self):
        return self.__exit_node

    def forward(self, x):
        x = self.__exit_node(x)
        x = self.log_softmax(x)
        return x


class Flatten(nn.Module):
    def forward(self, input):
        return input.reshape(input.size(0), -1)


class GraphNodeModule(nn.Module):

    ACT_FN_MAPPING = {
        'relu': nn.ReLU,
        'tanh': nn.Tanh
    }

    def __init__(self, graph_node, parameters, dependencies):
        super(GraphNodeModule, self).__init__()

        logger.debug("GraphNodeModule Layer: " + str(graph_node))

        self.__graph_node = graph_node
        self.__layer_type = graph_node.layer_type
        self.dependencies = dependencies
        self.__size_multiplier = self.__retrieve_size_multiplier(parameters)
        dataset_descriptor = retrieve_dataset(parameters['dataset'])
        self.__network_input_shape = dataset_descriptor.input_shape
        self.__network_input_size = dataset_descriptor.input_size
        self.__network_output_size = dataset_descriptor.output_size

        if self.__layer_type == Layer.DENSE:
            self.__layer_builder = self.__create_dense_layer_builder()
        elif self.__layer_type == Layer.CONVOLUTION:
            if self.__can_do_2d_convolution():
                self.__layer_builder = self.__create_conv2d_layer_builder()
            elif self.__can_do_1d_convolution():
                self.__layer_builder = self.__create_conv1d_layer_builder()
            else:
                self.__layer_type = Layer.DENSE
                self.__layer_builder = self.__create_dense_layer_builder()

        self.activation = self.__build_activation(parameters)

    def build(self, visited=None):
        if visited is None:
            visited = set()
        if self in visited:
            return
        visited.add(self)

        self.__layer_builder()

        for dependency in self.dependencies:
            dependency.build(visited)

    def __build_dense_layer(self):
        input_size = self.input_size()

        logger.debug(
            "Type: DENSE Input size: " + str(input_size) +
            " Output size: " + str(self.output_shape[0]) +
            " Network input size: " + str(self.__network_input_size) +
            " Network output size: " + str(self.__network_output_size) +
            " Neighbors: " + str(self.__graph_node.neighbors) +
            " " + str(len(self.__graph_node.neighbors))
        )

        self.process = nn.Linear(input_size, self.output_shape[0])

    def __create_dense_layer_builder(self):
        self.__output_shape = (self.output_size(),)
        return lambda: self.__build_dense_layer()

    def __build_conv2d_layer(self,
                             output_channels,
                             conv_kernel_size,
                             max_pooling_kernel_size,
                             conv_output_size,
                             width,
                             height):
        input_channels = self.__input_channels()

        logger.debug(
            "Type: Convolution 2d" +
            " Input channels: " + str(input_channels) +
            " Output channels " + str(output_channels) +
            " Input sample dims: " + str(width) + " " + str(height) +
            " Output sample dims: " + str(self.output_shape)
        )

        if self.__graph_node.is_exit_layer():
            self.process = nn.Sequential(
                nn.Conv2d(
                    input_channels,
                    output_channels,
                    conv_kernel_size
                ),
                nn.MaxPool2d(max_pooling_kernel_size),
                Flatten(),
                nn.Linear(conv_output_size, self.__network_output_size)
            )
        else:
            self.process = nn.Sequential(
                nn.Conv2d(
                    input_channels,
                    output_channels,
                    conv_kernel_size
                ),
                nn.MaxPool2d(max_pooling_kernel_size)
            )

    def __create_conv2d_layer_builder(self):
        output_channels = self.output_channels()

        if self.__has_one_dependency():
            width = self.dependencies[0].output_shape[1]
            height = self.dependencies[0].output_shape[2]
        else:
            width = self.__network_input_shape[1]
            height = self.__network_input_shape[2]

        if min(width, height) < 3:
            conv_kernel_size = 1
            max_pooling_kernel_size = 1
        else:
            conv_kernel_size = 3
            max_pooling_kernel_size = 2

        conv_output_shape = (
            output_channels,
            (width - (conv_kernel_size - 1)) // max_pooling_kernel_size,
            (height - (conv_kernel_size - 1)) // max_pooling_kernel_size
        )
        if self.__graph_node.is_exit_layer():
            self.__output_shape = (self.__network_output_size,)
        else:
            self.__output_shape = conv_output_shape

        return lambda: self.__build_conv2d_layer(output_channels,
                                                 conv_kernel_size,
                                                 max_pooling_kernel_size,
                                                 reduce(lambda l, r: l * r, conv_output_shape),
                                                 width,
                                                 height)

    def __build_conv1d_layer(self, output_channels, conv_kernel_size, max_pooling_kernel_size, conv_output_size):
        # there is only one channel - but with all previous layers'
        # outputs concatenated
        input_channels = 1

        logger.debug(
            "Type: Convolution 1d" +
            " Input channels: " + str(input_channels) +
            " Output channels " + str(output_channels)
        )

        if self.__graph_node.is_exit_layer():
            self.process = nn.Sequential(
                nn.Conv1d(
                    input_channels,
                    output_channels,
                    conv_kernel_size
                ),
                nn.MaxPool1d(max_pooling_kernel_size),
                Flatten(),
                nn.Linear(conv_output_size, self.__network_output_size)
            )
        else:
            self.process = nn.Sequential(
                nn.Conv1d(
                    input_channels,
                    output_channels,
                    conv_kernel_size
                ),
                nn.MaxPool1d(max_pooling_kernel_size)
            )

    def __create_conv1d_layer_builder(self):
        output_channels = self.output_channels()

        if self.input_size() < 3:
            conv_kernel_size = 1
            max_pooling_kernel_size = 1
        else:
            conv_kernel_size = 3
            max_pooling_kernel_size = 2

        if self.__graph_node.is_entry_layer():
            width = reduce(lambda l, r: l * r, self.__network_input_shape[1:])
        else:
            sizes = list(map(lambda d: d.output_size(), self.dependencies))
            width = sum(sizes)

        conv_output_shape = (
            output_channels,
            (width - (conv_kernel_size - 1)) // max_pooling_kernel_size
        )
        if self.__graph_node.is_exit_layer():
            self.__output_shape = (self.__network_output_size,)
        else:
            self.__output_shape = conv_output_shape

        return lambda: self.__build_conv1d_layer(output_channels,
                                                 conv_kernel_size,
                                                 max_pooling_kernel_size,
                                                 reduce(lambda l, r: l * r, conv_output_shape))

    def __build_activation(self, parameters):
        if 'activation_fn' in parameters:
            activation_fn = parameters['activation_fn']
            return GraphNodeModule.ACT_FN_MAPPING[activation_fn]()

        return nn.ReLU()

    @property
    def output_shape(self):
        return self.__output_shape

    @property
    def layer_type(self):
        return self.__layer_type

    @property
    def network_input_shape(self):
        return self.__network_input_shape

    @property
    def network_output_size(self):
        return self.__network_output_size

    def __can_do_2d_convolution(self):
        return (
            (self.__has_one_dependency() and self.__dependency_input_2d())
            or
            (self.__graph_node.is_entry_layer()
             and self.__network_input_is_2d())
        )

    def __can_do_1d_convolution(self):
        return (
                (self.__has_one_dependency() and self.__dependency_input_1d())
                or
                (self.__graph_node.is_entry_layer()
                 and self.__network_input_is_1d())
        )

    def __network_input_is_2d(self):
        return len(self.__network_input_shape) == 3

    def __network_input_is_1d(self):
        return len(self.__network_input_shape) == 2

    def __dependency_input_2d(self):
        if self.__graph_node.is_entry_layer():
            return self.__network_input_is_2d()

        return len(self.dependencies[0].output_shape) == 3

    def __dependency_input_1d(self):
        if self.__graph_node.is_entry_layer():
            return self.__network_input_is_1d()

        return len(self.dependencies[0].output_shape) == 2

    def __input_channels(self):
        if self.__graph_node.is_entry_layer():
            return self.__network_input_shape[0]

        if len(self.dependencies) == 1:
            return self.dependencies[0].output_shape[0]

        raise ValueError(
            "More than 1 dependency! Cannot do 2d convolution!"
        )

    def __retrieve_size_multiplier(self, parameters):
        if self.__layer_type == Layer.DENSE:
            return int(parameters['dense_size_multiplier'])

        if self.__layer_type == Layer.CONVOLUTION:
            return int(parameters['convolution_size_multiplier'])

        raise ValueError("Unknown layer type! Layer type: " +
                         str(self.__layer_type))

    def get_node_id(self):
        return self.__graph_node.id

    def output_size(self):
        if self.__graph_node.is_exit_layer():
            return self.__network_output_size

        if self.__layer_type == Layer.DENSE:
            extras = sum(self.__graph_node.extras)
            outputs_count = int(round(extras * self.__size_multiplier))
        elif self.__layer_type == Layer.CONVOLUTION:
            outputs_count = reduce(lambda l, r: l*r, self.output_shape)

        # edge case: if extras are 0 - we need to bump it at least to 1 neuron
        if outputs_count == 0:
            return 1

        return outputs_count

    def output_channels(self):
        if self.__graph_node.is_exit_layer():
            # if we are the last layer - return only 1 channel
            return 1

        extras = sum(self.__graph_node.extras)
        outputs_count = int(round(extras * self.__size_multiplier))

        # edge case: if extras are 0 - we need to bump it at least to 1 neuron
        if outputs_count == 0:
            return 1

        return outputs_count

    def input_size(self):
        if self.__graph_node.is_entry_layer():
            return self.__network_input_size

        dependency_outputs = [d.output_size() for d in self.dependencies]
        return sum(dependency_outputs)

    def __has_one_dependency(self):
        return len(self.dependencies) == 1

    def forward_dependencies(self, x):
        if len(self.dependencies) == 1:
            return self.dependencies[0].forward(x)

        dependency_results = []
        for dependency in self.dependencies:
            result = dependency.forward(x)
            # change the shape of the output to # of batches + a linear vector
            # for every dependency (in case there are >1) we need to change
            # their shape to enable concatenation per batch (we want to e.g.
            # concatenate 2d image from 2d convolution and a 1d vector from
            # a dense layer)
            dependency_results.append(result.reshape(result.size(0), -1))

        return self.__join_dependency_results(dependency_results)

    def __join_dependency_results(self, dependency_results):
        # dim=1 because dimension 0 is the number of batches
        # so now - we concat the output of previous layers for every sample in
        # a batch
        return torch.cat(dependency_results, dim=1)

    def has_dependencies(self):
        return len(self.dependencies) > 0

    def forward(self, x):
        if self.has_dependencies():
            x = self.forward_dependencies(x)

        if self.__layer_type == Layer.CONVOLUTION:
            if self.__can_do_2d_convolution():
                x = self.activation(self.process(x))
            else:
                x = self.activation(
                    self.process(
                        # first column, automatically adjusted - # of batches
                        # second column - # of channels
                        # third columns - # of inputs
                        x.reshape(-1, 1, self.input_size())
                    )
                )

                if self.__graph_node.is_exit_layer():
                    x = x.reshape(x.size(0), -1)
        else:
            # simple, flat set of inputs for a dense layer
            x = self.activation(self.process(x.reshape(-1, self.input_size())))
        return x

    def _get_name(self):
        return self.__class__.__name__

    def __repr__(self):
        prefix = " (Layer: " + str(self.__graph_node.id)
        if self.__graph_node.is_entry_layer():
            prefix += " I"

        if self.__graph_node.is_exit_layer():
            prefix += " O"

        prefix += ")"

        tmpstr = 'GraphNodeModule' + prefix + '(\n'
        deps = [
            ('dep{}'.format(str(i)), self.dependencies[i])
            for i in range(len(self.dependencies))
        ]

        def to_str(module):
            modstr = module.__repr__()
            modstr = _addindent(modstr, 2)
            return modstr

        for key, module in deps:
            modstr = to_str(module)
            tmpstr = tmpstr + '  (' + key + '): ' + modstr + '\n'

        for key, module in self.named_modules():
            if module != self:
                modstr = to_str(module)
                tmpstr = tmpstr + '  (' + key + '): ' + modstr + '\n'

        tmpstr = tmpstr + ')'
        return tmpstr


def wrap_with_nn_module(layer, parameters, graph_node_cache=None):
    if graph_node_cache is None:
        graph_node_cache = {}

    dependendency_modules = []
    for dependency in layer.dependencies:
        logger.debug("Wrapping dep: " + str(dependency))

        if dependency.id in graph_node_cache:
            logger.debug("Grabbing {} from cache".format(str(dependency)))
            wrapped = graph_node_cache[dependency.id]
        else:
            wrapped = wrap_with_nn_module(
                dependency,
                parameters,
                graph_node_cache
            )
            graph_node_cache[dependency.id] = wrapped

        dependendency_modules.append(wrapped)

    logger.debug("Wrapping: " + str(layer.id))
    wrapped = GraphNodeModule(layer, parameters, dependendency_modules)
    graph_node_cache[layer.id] = wrapped
    return wrapped


def populate_dependencies(entry_node):
    """
        This should work without a problem, given that the graph won't have any
        cycles.

        Building the network based on the dependencies instead of neighbors (in
        graph terms neighbors are forward connections, dependencies - backward)
        has one more advantage: after disconnecting all cycles, there might be
        some nodes/layers floating around without connection to the last, exit
        layer. When we go backwards from the exit to entry we won't add such
        layers.
    """
    queue = [entry_node]
    while len(queue) > 0:
        node = queue.pop(0)

        for neighbor in node.neighbors:
            neighbor.add_dependency(node)

            if neighbor in queue:
                continue

            queue.append(neighbor)


def remove_unnecessary_connections(entry_node):
    visited = set()
    queue = [entry_node]
    while len(queue) > 0:
        layer = queue.pop(0)
        visited.add(layer)

        to_visit = layer.neighbors[:]
        for neighbor in to_visit:
            if neighbor in visited:
                # if the neighbor was already visited this means that there
                # is a cycle - we want to break it
                layer.remove_neighbor(neighbor)
                continue

            if neighbor in queue:
                # avoid scheduling the same node more than once
                # if a node already went through the queue - it is already
                # visited
                continue

            queue.append(neighbor)


def connect_nearest_layer(layers, genes_to_layers, connected_layers, genotype):
    not_connected_input_genes_ids = set()
    not_connected_layers = layers - connected_layers
    min_distance_to_gene = math.inf
    nearest_layer_input = None
    nearest_layer_output = None

    for layer in not_connected_layers:
        for gene_id in layer.gene_ids:
            if genotype.gene(gene_id).is_input():
                not_connected_input_genes_ids.add(gene_id)

    for layer in connected_layers:
        for gene_id in layer.gene_ids:
            gene = genotype.gene(gene_id)
            if gene.is_output:
                for other_gene_id in not_connected_input_genes_ids:
                    distance = gene.distance_sqr_to(genotype.gene(other_gene_id))
                    if distance < min_distance_to_gene:
                        min_distance_to_gene = distance
                        nearest_layer_input = genes_to_layers[other_gene_id]
                        nearest_layer_output = layer

    nearest_layer_output.add_neighbor(nearest_layer_input)
    return nearest_layer_input


def connect_entry_with_exit(layers, genes_to_layers, genotype):
    connected_layers = get_connected_layers(genes_to_layers[0])
    while genes_to_layers[-1] not in connected_layers:
        newly_connected_layer = connect_nearest_layer(layers, genes_to_layers, connected_layers, genotype)
        connected_layers = get_connected_layers(newly_connected_layer, connected_layers)


# Use DFS to get a set of all layers connected to layer passed as argument
# `visited` set of layers can be passed as an argument to not visit already visited layers from previous call.
def get_connected_layers(layer, visited=None):
    if visited is None:
        visited = set()
    elif layer in visited:
        return set()
    visited.add(layer)
    for neighbor_layer in layer.neighbors:
        visited.update(get_connected_layers(neighbor_layer, visited))
    return visited


def __create_optimizer(config_parameters, model_parameters):
    learning_rate = float(config_parameters['learning_rate'])
    optimizer_type = config_parameters.get('optimizer', 'sgd')
    if optimizer_type == 'adam':
        logger.debug('Using optimizer: adam')
        optimizer = optim.Adam(
            model_parameters,
            lr=learning_rate
        )
    elif optimizer_type == 'amsgrad':
        logger.debug('Using optimizer: amsgrad')
        optimizer = optim.Adam(
            model_parameters,
            lr=learning_rate,
            amsgrad=True
        )
    else:
        logger.debug('Using optimizer: sgd')
        momentum = float(config_parameters['momentum'])
        optimizer = optim.SGD(
            model_parameters,
            lr=learning_rate,
            momentum=momentum
        )

    return optimizer


def __train_model(model, train_loader, parameters, progress_tracker):
    iterations = int(parameters['train_iterations'])

    batches_trained = 0

    optimizer = __create_optimizer(parameters, model.parameters())
    log_message = 'Training: iteration {} [btch: {}/{} ({:.0f}%)] Loss: {:.6f}'

    for i in range(iterations):
        logger.info("Training: iteration {}/{}".format(i, iterations))
        # set the model mode to 'train'
        model.train()

        # enumerate over batched dataset
        for batch_idx, (data, target) in enumerate(train_loader):
            # convert tensors to variables
            data, target = Variable(data), Variable(target)

            # clean up gradient variables
            optimizer.zero_grad()

            # forward pass
            output = model(data)

            # calculate loss function
            # TODO: parametrize: enable different loss functions
            loss = F.nll_loss(output, target)

            # calculate gradients
            loss.backward()

            # update weights
            optimizer.step()

            # log results
            logger.debug(log_message.format(
                        i,
                        batch_idx,
                        len(train_loader),
                        100. * batch_idx / len(train_loader),
                        loss.data.item()))

            batches_trained += 1
            progress_tracker.update_progress(batches_trained)


def __test_model(model, test_loader, parameters):
    model.eval()

    log_message = 'Test set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'
    test_loss = 0
    instances_per_class = defaultdict(int)
    correct_per_class = defaultdict(int)
    correct = 0
    calculate_accuracy_per_class = parameters['calculate_accuracy_per_class']

    for data, target in test_loader:
        output = model(data)

        # sum up batch loss
        # TODO: parametrize: enable different loss functions
        test_loss += F.nll_loss(output, target, reduction="sum").data.item()

        # get the index of the max log-probability
        pred = output.data.max(1, keepdim=True)[1]
        # view_as rearranges target.data to size of pred tensor
        # eq outputs 1 or 0
        # then we sum it - count times algorithm was right
        pred_correct = pred.eq(target.data.view_as(pred))
        correct += pred_correct.cpu().sum()

        if calculate_accuracy_per_class:
            for i in range(len(target)):
                instances_per_class[target[i].item()] += 1
                correct_per_class[target[i].item()] += int(pred_correct[i].item())

    test_loss /= len(test_loader.dataset)
    accuracy = 100. * correct.item() / float(len(test_loader.dataset))

    logger.info(
        log_message.format(
            test_loss,
            correct,
            len(test_loader.dataset),
            accuracy
        )
    )

    if calculate_accuracy_per_class:
        accuracy_per_class = []
        for c in range(len(instances_per_class)):
            accuracy_per_class.append(100. * correct_per_class[c] / float(instances_per_class[c]))
        return NoveltyEvaluationResult(accuracy=accuracy, accuracy_per_class=np.array(accuracy_per_class))
    return EvaluationResult(accuracy=accuracy)


def prepare_sampler(dataset_subset):
    if dataset_subset is not None and len(dataset_subset) > 0:
        return SubsetRandomSampler(dataset_subset)

    return None


def evaluate_model_accuracy(
    model,
    dataset_subset,
    parameters,
    progress_tracker=None
):
    progress_tracker = progress_tracker or IgnoringProgressTracker()
    sampler = prepare_sampler(dataset_subset)
    batch_size = int(parameters['batch_size'])
    dataset_name = parameters['dataset']
    dataset = retrieve_dataset(dataset_name)
    train_dataset = dataset.torch_train_dataset
    test_dataset = dataset.torch_test_dataset
    train_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=batch_size,
        shuffle=False,
        sampler=sampler
    )
    test_loader = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=batch_size,
        shuffle=False
    )

    logger.debug("Evaluating model with id: " + str(id(model)))

    total_batches = int(parameters['train_iterations']) * len(train_loader)
    progress_tracker.set_target_value(total_batches)

    __train_model(model, train_loader, parameters, progress_tracker)
    result = __test_model(model, test_loader, parameters)

    return result


def translate_to_model(individual, parameters):
    genotype = Genotype(list(individual))
    genes_to_layers = genotype_to_layers(genotype)
    adjacency_matrix = genes_adjacency_matrix(genotype)

    layers = set(genes_to_layers)

    for layer in layers:
        if not layer.is_exit_layer():
            for gene_id in layer.gene_ids:
                if genotype.gene(gene_id).is_output():
                    for i in range(genotype.size):
                        if gene_id != i\
                                and genotype.gene(i).is_input()\
                                and adjacency_matrix[gene_id][i] == 1:
                            neighbor_layer = genes_to_layers[i]
                            layer.add_neighbor(neighbor_layer)

    # make sure there is connection between entry and exit layer
    connect_entry_with_exit(layers, genes_to_layers, genotype)

    # this should remove all references "backwards" and loops
    input_node = genes_to_layers[0]
    output_node = genes_to_layers[-1]

    remove_unnecessary_connections(input_node)

    populate_dependencies(input_node)

    for layer in layers:
        logger.debug("Layer: {} neighbors: {} dependencies: {}".format(
            layer.id,
            [str(neigh.id) for neigh in layer.neighbors],
            [str(dep.id) for dep in layer.dependencies]
        ))

    logger.debug("Input layer: " + str(input_node))
    logger.debug("Output layer: " + str(output_node))

    graph_node_module = wrap_with_nn_module(output_node, parameters)
    return GraphModule(graph_node_module, parameters)


def initialize_rng(parameters):
    seed = int(parameters['rng_seed'])
    if seed >= 0:
        random.seed(seed)
        torch.manual_seed(seed)


def evaluate_as_nn(
    individual,
    parameters,
    dataset_subset=None,
    progress_tracker=None
):

    logger.info(
        "Evaluating genotype: {} Parameters: {} Dataset subset: {}".format(
            individual,
            parameters,
            dataset_subset
        )
    )

    initialize_rng(parameters)
    nn_module = translate_to_model(individual, parameters)
    nn_module.build()
    result = evaluate_model_accuracy(
        model=nn_module,
        dataset_subset=dataset_subset,
        parameters=parameters,
        progress_tracker=progress_tracker,
    )
    del nn_module

    return result
