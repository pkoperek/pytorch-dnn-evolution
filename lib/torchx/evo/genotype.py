from typing import List

import torch
import random

Code = List[float]

NUMBER_OF_FIELDS = 5
NUMBER_OF_EXTRA_FIELDS = 3  # first and last genes are added inside genotype and need extra fields
OUTPUT_TYPE_THRESHOLD = 0.5

genotype_id = 0


def get_id():
    global genotype_id
    genotype_id += 1
    return genotype_id


def genes_distance_matrix(genotype):
    xs = torch.Tensor(
        [genotype.gene(i).x for i in range(genotype.size)]
    )
    ys = torch.Tensor(
        [genotype.gene(i).y for i in range(genotype.size)]
    )

    length = xs.size(0)

    x = xs.expand(length, length)
    y = ys.expand(length, length)
    x_t = torch.t(x)
    y_t = torch.t(y)

    distances = torch.sqrt((x - x_t) * (x - x_t) + (y - y_t) * (y - y_t))

    return distances


def cut_off_distance(distance_matrix):
    """
        Average distance in the matrix
    """
    return torch.sum(distance_matrix) / distance_matrix.nelement()


def genes_adjacency_matrix(genotype):
    distance_matrix = genes_distance_matrix(genotype)
    cut_off_threshold = cut_off_distance(distance_matrix)

    # = required for edge cases like having only two layers
    return distance_matrix <= cut_off_threshold


class Gene(object):
    """
        There are two types of genes - INPUT and OUTPUT ones.
        INPUT - denotes input to a layer.
        OUTPUT - denotes output from a layer
    """
    INPUT = 1
    OUTPUT = 0

    def __init__(
            self,
            x,
            y,
            gene_type_value,
            extra,
            layer_type
            ):
        self.__x = x
        self.__y = y
        self.__extra = extra
        self.__gene_type_value = gene_type_value
        self.__gene_type = Gene.OUTPUT \
            if gene_type_value < OUTPUT_TYPE_THRESHOLD\
            else Gene.INPUT
        self.__layer_type = layer_type

    @property
    def layer_type(self):
        return self.__layer_type

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    @property
    def type_value(self):
        return self.__gene_type_value

    @property
    def extra(self):
        return self.__extra

    @property
    def gene_type(self):
        return self.__gene_type

    def distance_sqr_to(self, other):
        return (self.x - other.x) * (self.x - other.x) + (self.y - other.y) * (self.y - other.y)

    def is_output(self):
        return self.__gene_type == Gene.OUTPUT

    def is_input(self):
        return self.__gene_type == Gene.INPUT


def random_floats(size):
    return [random.random() for _ in range(size)]


class Genotype(object):

    ENTRY_POINT = [0.0, 0.0, OUTPUT_TYPE_THRESHOLD-0.01, 0.0, 0.0]
    EXIT_POINT = [1.0, 1.0, OUTPUT_TYPE_THRESHOLD+0.01, 0.0, 0.0]

    def __init__(self, code: Code):
        self.__id = get_id()
        entry_point = self.__entry_point(extra=code[0], layer_type=code[1])
        exit_point = self.__exit_point(extra=code[2])
        self.__code = entry_point + code[NUMBER_OF_EXTRA_FIELDS:] + exit_point

    def __entry_point(self, extra, layer_type):
        copy = Genotype.ENTRY_POINT[:]
        copy[3] = extra
        copy[4] = layer_type
        return copy

    def __exit_point(self, extra):
        copy = Genotype.EXIT_POINT[:]
        copy[3] = extra
        # exit layer should always be dense
        copy[4] = 0.0
        return copy

    def gene(self, idx):
        return Gene(
            self.__code[idx * NUMBER_OF_FIELDS],
            self.__code[idx * NUMBER_OF_FIELDS + 1],
            self.__code[idx * NUMBER_OF_FIELDS + 2],
            self.__code[idx * NUMBER_OF_FIELDS + 3],
            self.__code[idx * NUMBER_OF_FIELDS + 4],
        )

    @property
    def size(self):
        return int(self.raw_size / NUMBER_OF_FIELDS)

    @property
    def raw_size(self):
        return len(self.__code)

    @property
    def code(self):
        return self.__code

    @property
    def id(self):
        return self.__id

    @staticmethod
    def random(size):
        code = random_floats(NUMBER_OF_EXTRA_FIELDS + size)
        return Genotype(code=code)

    def clone(self, new_raw_code):
        return Genotype(new_raw_code)
