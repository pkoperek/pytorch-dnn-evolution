from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import TypeVar, Generic, Tuple, List, Union
from torchx.evo.evaluation_result import EvaluationResult

T = TypeVar('T')


class ValuePromise(ABC, Generic[T]):
    @abstractmethod
    def get_value(self) -> Union[T, Tuple[T, ...]]: ...


@dataclass
class MemoryValuePromise(Generic[T], ValuePromise[T]):
    _value: T

    def get_value(self) -> T:
        return self._value


@dataclass
class TargetDiffValuePromise(Generic[T], ValuePromise[T]):
    _promise: ValuePromise[T]
    _target: T

    def get_value(self) -> Tuple[T]:
        result = self._promise.get_value()
        target_diff = abs(self._target - result.accuracy)
        return target_diff
    
    def get_cpu_time_s(self) -> Tuple[float]:
        return self._promise.get_cpu_time_s()


@dataclass
class AvgCompositeValuePromise(ValuePromise[Union[int, float, EvaluationResult]]):
    _promises: List[ValuePromise[Union[int, float, EvaluationResult]]]

    def get_value(self) -> Tuple[float]:
        results_cnt = len(self._promises)

        results = [promise.get_value() for promise in self._promises]

        # assuming that every result has the same type
        result_type = type(results[0])

        results_sum = sum(results, start=result_type())
        avg = results_sum / float(results_cnt)

        return avg
    
    def get_total_cpu_time_s(self) -> Tuple[float]:
        results = [promise.get_cpu_time_s() for promise in self._promises]

        return sum(results)
