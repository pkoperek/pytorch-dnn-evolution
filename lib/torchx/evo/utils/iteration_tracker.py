from abc import ABC, abstractmethod
from contextlib import contextmanager
import logging
from typing import TypeVar, Generic, Callable, List, NamedTuple, Tuple, Any

T = TypeVar('T')
Listener = Callable[[T], Any]


class Notificator(Generic[T]):
    Ref = Any

    def __init__(self):
        self._listeners = []

    def add_listener(self, listener: Listener[T]) -> Ref:
        self._listeners.append(listener)
        return listener

    def remove_listener(self, listener: Ref):
        self._listeners.remove(listener)

    def listen_to(self, other: 'Notificator[T]', decorator: Callable[[T], T] = lambda x: x) -> Ref:
        return other.add_listener(lambda v: self._notify(decorator(v)))

    def _notify(self, value: T):
        if not self._listeners:
            logging.getLogger('notificator').warning('Nobody is listening to progress tracking!')

        for listener in self._listeners:
            listener(value)


class IterationStatus(NamedTuple):
    current_no: int
    current_progress: int
    name: str


class IterationTracker(ABC, Notificator[IterationStatus]):
    def __init__(self, name):
        super().__init__()
        self._name = name

    @property
    @abstractmethod
    def is_active(self) -> bool: ...

    @property
    @abstractmethod
    def current_no(self) -> int: ...

    @property
    @abstractmethod
    def current_progress(self) -> int: ...

    @property
    @abstractmethod
    def current_target(self) -> int: ...

    @property
    def name(self) -> str:
        return self._name

    def status(self) -> IterationStatus:
        return IterationStatus(
            current_no=self.current_no,
            current_progress=self.current_progress,
            name=self.name
        )


class StatefulIterationTracker(IterationTracker):
    def __init__(self, log, name, initial_no: int = 0, initial_target: int = 0):
        super().__init__(name)

        self._current_no = initial_no
        self._current_target = initial_target
        self._current_ticks = 0
        self._log = log
        self._active = False

    @property
    def is_active(self) -> bool:
        return self._active

    @property
    def current_no(self) -> int:
        return self._current_no

    @property
    def current_progress(self) -> int:
        # if nothing to evaluate - we are done
        if self._current_target == 0:
            return 100

        return int(100 * (self._current_ticks / self._current_target))

    @property
    def current_target(self) -> int:
        return self._current_target

    @contextmanager
    def enter(self) -> 'IterationSection':
        self._log.debug(f"Iteration {self.current_no} start")
        self._active = True

        yield self.IterationSection(self)

        self._current_ticks = 0
        self._current_no += 1
        self._active = False

        self._log.debug(f"Iteration {self.current_no} end")

    class IterationSection:
        def __init__(self, tracker: 'StatefulIterationTracker'):
            self._tracker = tracker

        def set_target(self, target: int):
            self._tracker._current_target = target
            self._tracker._log.debug(f"To evaluate: {self._tracker._current_target}")

        def tick(self) -> None:
            self._tracker._current_ticks += 1
            self._tracker._notify(self._tracker.status())


class CompositeIterationTracker(IterationTracker):
    steps: List[IterationTracker]

    def __init__(self, log, name, steps: List[IterationTracker]):
        super().__init__(name)

        self._log = log

        self._steps = steps
        for step in self._steps:
            self.listen_to(step, lambda _: self.status())
        self._in_special_section = False

    @property
    def is_active(self) -> bool:
        return True

    @property
    def current_no(self) -> int:
        iteration_no = min(it.current_no for it in self._steps)
        if self._in_special_section:
            return iteration_no - 1
        return iteration_no

    @property
    def current_progress(self) -> int:
        step = self.current_step
        return step.current_progress

    @property
    def name(self) -> str:
        step = self.current_step
        return step.name

    @property
    def current_target(self) -> int:
        step = self.current_step
        return step.current_target

    @property
    def current_step(self) -> IterationTracker:
        for i, step in enumerate(self._steps):
            if step.is_active:
                return step

        return self._steps[0]

    @contextmanager
    def with_special_section(self) -> StatefulIterationTracker.IterationSection:
        step = StatefulIterationTracker(self._log, "Selecting new FP trainers", initial_no=self.current_no)
        self.listen_to(step, lambda _: self.status())

        prev_steps = self._steps
        self._steps = [step]
        try:
            with step.enter() as section:
                self._in_special_section = True
                yield section
        finally:
            self._steps = prev_steps
            self._in_special_section = False
