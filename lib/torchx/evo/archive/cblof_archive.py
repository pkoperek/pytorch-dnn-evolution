from torchx.evo.archive.pyod_archive import PyODArchive
from torchx.evo.evolution.configuration import NoveltyComparisonMethod
from pyod.models.cblof import CBLOF


class CblofArchive(PyODArchive):
    def __init__(self, compare_to: NoveltyComparisonMethod):
        super().__init__(compare_to)
        self._model = CBLOF()
