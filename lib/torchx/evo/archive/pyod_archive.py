from abc import ABC
import numpy as np
from typing import Union, Optional
import logging
from pyod.models.base import BaseDetector
from torchx.evo.evolution.configuration import NoveltyComparisonMethod


class ArchiveTooSmallException(Exception):
    """Exception raised when the archive has an insufficient
        number of elements and cannot calculate the novelty score.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message="Archive is too small and cannot calculate novelty score."):
        self.message = message
        super().__init__(self.message)


class PyODArchive(ABC):
    """Abstract of an archive using PyOD proximity modules.

    Every archive using PyOD proximity modules should be a child of this class.
    """

    def __init__(self, compare_to: NoveltyComparisonMethod):
        self._archive = np.array([])
        self._model = Optional[BaseDetector]
        self._compare_to = compare_to
        logger = logging.getLogger(__name__)
        logger.debug(f"Created archive of type: {self.__class__.__name__}")


    def insert(self, results: Union[list[np.ndarray], np.ndarray]):
        """Add new elements to archive

        Args:
            results (Union[list[np.ndarray], np.ndarray]): Array which is basis of calculating distance
                (e.g. accuracy for each class).
        """
        results = np.atleast_2d(results)
        if len(self._archive) == 0:
            self._archive = results
            return
        self._archive = np.vstack((self._archive, results))

    def can_get_novelty(self, results: Union[list[np.ndarray], np.ndarray]):
        """Check if novelty scores can be calculated

        Returns:
            True if novelty score can be calculated, False otherwise
        """
        if self._compare_to == NoveltyComparisonMethod.PREV_GENS:
            return len(self._archive) > 0
        if self._compare_to == NoveltyComparisonMethod.ALL:
            return True
        if self._compare_to == NoveltyComparisonMethod.ALL_BESIDES_SELF:
            return len(self._archive) + len(results) - 1 > 0

    def get_novelty(self, results: Union[list[np.ndarray], np.ndarray]):
        """Calculate novelty scores of given results

        Args:
            results (np.ndarray): 2D Array of shape (number of results, number of results' dimensions),
                which is the basis for calculating novelty (e.g. accuracy for each class).

        Returns:
            Novelty score of given results (np.ndarray of shape (number of results) containing floats between 0 and 1)
        """
        if not self.can_get_novelty(results):
            raise ArchiveTooSmallException()

        results = np.atleast_2d(results)
        if len(self._archive) == 0:
            self._archive = np.empty((0, results.shape[1]))

        if self._compare_to == NoveltyComparisonMethod.PREV_GENS:
            return self.get_novelty_comp_to_prev_gens(results)
        if self._compare_to == NoveltyComparisonMethod.ALL:
            return self.get_novelty_comp_to_all(results)
        if self._compare_to == NoveltyComparisonMethod.ALL_BESIDES_SELF:
            return self.get_novelty_comp_to_all_besides_self(results)

    def get_novelty_comp_to_prev_gens(self, results: np.ndarray):
        self._model.fit(self._archive)
        return self._model.predict_proba(results)[:, 1]

    def get_novelty_comp_to_all(self, results: np.ndarray):
        self._model.fit(np.vstack((self._archive, results)))
        return self._model.predict_proba(results)[:, 1]

    def get_novelty_comp_to_all_besides_self(self, results: np.ndarray):
        novelty = np.empty(len(results))
        for i in range(len(results)):
            fitting_data = np.vstack((self._archive, results[:i], results[i+1:]))
            self._model.fit(fitting_data)
            novelty[i] = self._model.predict_proba(np.atleast_2d(results[i]))[:, 1]

        return novelty
