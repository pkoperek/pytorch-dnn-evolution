from torchx.evo.archive.pyod_archive import PyODArchive
from torchx.evo.evolution.configuration import NoveltyComparisonMethod
from pyod.models.knn import KNN
from typing import Union
import numpy as np


class KnnArchive(PyODArchive):
    """
    KNN-based novelty archive.
    It's important to note that this archive calculates novelty based on the distance
    to the n_neighbors nearest neighbors of queried result. Because of this, the novelty scores
    cannot be calculated if the number of samples in the archive is less than n_neighbors.
    """

    def __init__(
        self,
        compare_to: NoveltyComparisonMethod,
        contamination=0.1,
        n_neighbors=5,
        method="largest",
        radius=1.0,
        algorithm="auto",
        leaf_size=30,
        metric="minkowski",
        p=2,
        metric_params=None,
        n_jobs=1,
    ):
        super().__init__(compare_to)
        self._n_neighbors = n_neighbors
        self._model = KNN(
            contamination=contamination,
            n_neighbors=n_neighbors,
            method=method,
            radius=radius,
            algorithm=algorithm,
            leaf_size=leaf_size,
            metric=metric,
            p=p,
            metric_params=metric_params,
            n_jobs=n_jobs,
        )

    def can_get_novelty(self, results: Union[list[np.ndarray], np.ndarray]):
        """Check if novelty scores can be calculated

        Returns:
            True if novelty score can be calculated, False otherwise
        """
        if self._compare_to == NoveltyComparisonMethod.PREV_GENS:
            return len(self._archive) >= self._n_neighbors
        if self._compare_to == NoveltyComparisonMethod.ALL:
            return len(self._archive) + len(results) >= self._n_neighbors
        if self._compare_to == NoveltyComparisonMethod.ALL_BESIDES_SELF:
            return len(self._archive) + len(results) - 1 >= self._n_neighbors
