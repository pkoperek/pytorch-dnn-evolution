from __future__ import annotations

from torchx.evo.archive.base_archive import BaseArchive
from typing import Callable, Union
import numpy as np


class SimpleArchive(BaseArchive):
    """Simplest archive using euclidian metric and nearest neighbors.

    This class is the implementation of simplest version of archive. It's
    not optimized, just meant to work reliably.

    """

    def __init__(
        self,
        threshold: float,
        k: int,
        epsilon: float = 1e-7,
        distance: Callable = lambda a, b: np.linalg.norm(a - b),
    ):
        """

        Args:
            threshold (float): The minimal distance between that new element
                has to have to all others to be inserted.
             k (int): number of neighbors to return
            epsilon (float): How close the distance between two points has to be, for
                them to be considered th same point.
            distance (Callable): Metric to calculate distance between to points, by default,
                euclidian metric is used. The callable has to take two arguments (two points).

        """
        super().__init__(threshold, k)
        self.archive = []
        self.distance = distance
        self.epsilon = epsilon

    def insert(self, results: Union[list[np.ndarray], np.ndarray]):
        """Add new element to archive

        Args:
            results (Union[list[np.ndarray], np.ndarray]): Array which is basis of calculating distance
                (e.g. accuracy for each class).

        """
        for result in results:
            add = True
            for other in self.archive:
                if self.distance(other, result) < self.threshold:
                    add = False
                    break
            if add:
                self.archive.append(result)

    def force_insert(self, results: Union[list[np.ndarray], np.ndarray]):
        """Add new elements to archive without looking at threshold

        Args:
            results (Union[list[np.ndarray], np.ndarray]): Array of arrays which are basis of calculating distance
                (e.g. accuracy for each class).
        """
        results = self.__adjust_results_type(results)
        self.archive += results

    def get_nearest_neighbours(self, results: np.ndarray) -> list:
        """Calculate nearest neighbours of a point

        Args:
            results (np.ndarray): Array which is basis of calculating distance
                (e.g. accuracy for each class).

        Returns:
            Array of distances to k closest neighbors.

        """
        distances = []
        for other in self.archive:
            distances.append(self.distance(other, results))
        distances.sort()
        k_1 = distances[0 : (self.k + 1)]
        if len(k_1) == 0:
            return k_1
        if self.distance(results, k_1[0]):
            return k_1[:-1]
        return k_1[1:]

    def clear(self):
        """ Clear all elements in archive
        """
        self.archive.clear()

    def extend(self, other: SimpleArchive):
        """ Add elements form other archive

        Args:
            other (SimpleArchive): Other archive that is merged with this one.
        """
        self.archive.extend(other.archive)

    @staticmethod
    def __adjust_results_type(results):
        if type(results) == np.ndarray:
            return [res for res in results]
        return results
