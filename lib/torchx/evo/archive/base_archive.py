from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Union
import numpy as np


class BaseArchive(ABC):
    """Abstract of archive.

    Each archive should be a child of this class.

    """

    def __init__(self, threshold: float, k: int):
        """

        Args:
            threshold (float): The minimal distance between that new element
                has to have to all others to be inserted.
             k (int): number of neighbors to return

        """
        self.threshold = threshold
        self.k = k

    @abstractmethod
    def insert(self, results: Union[list[np.ndarray], np.ndarray]):
        """Add new elements to archive

        Args:
            results (Union[list[np.ndarray], np.ndarray]): Array of arrays which are basis of calculating distance
                (e.g. accuracy for each class).
        """
        pass

    @abstractmethod
    def force_insert(self, results: Union[list[np.ndarray], np.ndarray]):
        """Add new elements to archive without looking at threshold

        Args:
            results (Union[list[np.ndarray], np.ndarray]): Array of arrays which are basis of calculating distance
                (e.g. accuracy for each class).
        """
        pass

    @abstractmethod
    def get_nearest_neighbours(self, results: np.ndarray) -> np.ndarray:
        """Calculate nearest neighbours of a point

        Args:
            results (np.ndarray): Array which is basis of calculating distance
                (e.g. accuracy for each class).

        Returns:
            Array of distances to k closest neighbors.

        """
        pass

    @abstractmethod
    def clear(self):
        """ Clear all elements in archive
        """
        pass

    @abstractmethod
    def extend(self, other: BaseArchive):
        """ Add elements form other archive

        Args:
            other (BaseArchive): Other archive that is merged with this one.
        """
        pass

